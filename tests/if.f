[
    {
        "opcode": "function",
        "name": 1,
        "body-size": 14
    },
    {
        "opcode": "dup"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "1"
    },
    {
        "opcode": "="
    },
    {
        "opcode": "if",
        "true-size": 4,
        "false-size": 4
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "2"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "2"
    },
    {
        "opcode": "*"
    },
    {
        "opcode": ";"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "2"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "2"
    },
    {
        "opcode": "/"
    },
    {
        "opcode": ";"
    },
    {
        "opcode": "+"
    },
    {
        "opcode": ";"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "4"
    },
    {
        "opcode": "word",
        "type": "func",
        "value": 1
    },
    {
        "opcode": "."
    }
]