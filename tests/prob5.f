[
    {
        "opcode": "function",
        "name": 1,
        "body-size": 8
    },
    {
        "opcode": "begin",
        "begin-size": 2,
        "while-size": 3
    },
    {
        "opcode": "dup"
    },
    {
        "opcode": "while"
    },
    {
        "opcode": "tuck"
    },
    {
        "opcode": "mod"
    },
    {
        "opcode": "repeat"
    },
    {
        "opcode": "drop"
    },
    {
        "opcode": ";"
    },
    {
        "opcode": "function",
        "name": 2,
        "body-size": 7
    },
    {
        "opcode": "over"
    },
    {
        "opcode": "over"
    },
    {
        "opcode": "*"
    },
    {
        "opcode": "-rot"
    },
    {
        "opcode": "word",
        "type": "func",
        "value": 1
    },
    {
        "opcode": "/"
    },
    {
        "opcode": ";"
    },
    {
        "opcode": "function",
        "name": 3,
        "body-size": 10
    },
    {
        "opcode": "over"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "1"
    },
    {
        "opcode": "="
    },
    {
        "opcode": "if",
        "true-size": 2,
        "false-size": 3
    },
    {
        "opcode": "*"
    },
    {
        "opcode": ";"
    },
    {
        "opcode": "word",
        "type": "func",
        "value": 2
    },
    {
        "opcode": "word",
        "type": "func",
        "value": 3
    },
    {
        "opcode": ";"
    },
    {
        "opcode": ";"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "1"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "2"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "3"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "4"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "5"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "6"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "7"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "8"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "9"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "10"
    },
    {
        "opcode": "word",
        "type": "func",
        "value": 3
    },
    {
        "opcode": "."
    }
]