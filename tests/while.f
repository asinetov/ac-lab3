[
    {
        "opcode": "function",
        "name": 1,
        "body-size": 7
    },
    {
        "opcode": "begin",
        "begin-size": 3,
        "while-size": 2
    },
    {
        "opcode": "dup"
    },
    {
        "opcode": "+"
    },
    {
        "opcode": "while"
    },
    {
        "opcode": "*"
    },
    {
        "opcode": "repeat"
    },
    {
        "opcode": ";"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "0"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "1"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "2"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "3"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "4"
    },
    {
        "opcode": "word",
        "type": "func",
        "value": 1
    },
    {
        "opcode": "."
    }
]