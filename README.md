# Архитектура компьютера. Лабораторная 3

- Синетов Айдар, группа Р33131
- Вариант `forth | stack | harv | hw | instr | struct | trap | mem | prob5`

Код выполняется последовательно

## Система команд

Особенности процессора:

- Память данных:
    - Адресуется через регистр `data_address`, в который записывается значение из `Control Unit`
    - Пространство памяти, отвечающее за ввод и вывод адрессуется регистрами `input_pointer`, `output_pointer` (циклически автоинкрементируются при чтении и записи)
- Два стека
  - Стек данных (управляется регистрами `stack_buffer` и `stack_pointer`) 
  - Стек возвратов (в нем хранится `backtrace` программы)
- `program_counter` -- счётчик команд:
  - инкрементируется после каждой инструкции или перезаписывается инструкцией перехода.

### Набор инструкций
| Syntax   | Mnemonic | Кол-во тактов | Comment                                                                                   |
|:---------|:---------|---------------|:------------------------------------------------------------------------------------------|
| `:...;`  | function | 1             | Определение функции                                                                       |
| `dup`    | dup      | 2             | дублировать первое значение на стеке                                                      |
| `over`   | over     | 2             | дублировать второе значение на стеке                                                      |
| `drop`   | drop     | 1             | выкинуть первое значение со стека                                                         |
| `+`      | `plus`   | 3             | взять первые два числа со стека, и записать на стек результат                             |
| ...      | ...      | 3             | другие арифметические операции                                                            |
| `swap`   | `swap`   | 5             | поменять два значение на стеке местами                                                    |
| `tuck`   | `tuck`   | 7             | `~ swap over`                                                                             |
| `rot`    | `rot`    | 8             | stack: `n1 n2 n3` -> stack `n2 n3 n1`                                                     |
| `-rot`   | `-rot`   | 8             | stack: `n1 n2 n3` -> stack `n3 n1 n2`                                                     |
| число    | `word`   | 1             | положить значение на стек                                                                 |
| `word`   | `word`   | 2             | вызвать функцию                                                                           |
| `if`     | `if`     | 3             | взять значение со стека и в зависимости от его истинности, переключиться на одну из веток |
| `begin`  | `begin`  | 3             | сохранить контекст                                                                        |
| `while`  | `while`  | 3             | взять со стека условие, и если оно ложно, восстановить контекст и прыгнуть на конец цикла |
| `repeat` | `repeat` | 3             | вернуться в начало цикла                                                                  |
| `repeat` | `repeat` | 3             | вернуться в начало цикла                                                                  |
| `KEY`    | `KEY`    | 1             | прочитать один байт с ввода                                                               |
| `CR`     | `CR`     | 1             | вывести один байт                                                                         |
| `;`      | `end`    | 1             | восстановить контекст                                                                     |
| `.`      | `hlt`    | 1             | завершить исполнение                                                                      |


### Кодирование инструкций 
- Машинный код сериализуется в список JSON.
- Один элемент списка, одна инструкция.
- Для команд перехода используются идентификаторы
Пример:
```
{
   "opcode": "function",
    "name": "0",
    "body-size": 15
},
{
    "opcode": "word",
    "value": "4"
},
{
    "opcode": "dup"
},
{
    "opcode": "word",
    "value": "1",
    "type": "int" 
}
```

- `opcode` -- строка с кодом операции;
- `value` -- аргумент для слов (опц)
- `name` -- id функции (опц)
- Параметры размера функций и условных операторов

## Транслятор
Реализовано в модуле: [translator](./translator.py).

Интерфейс командной строки: `python translator.py <input_file> <target_file>`

Этапы трансляции (функция translate):
1. Трансформирование текста в последовательность значимых термов.
2. Проверка корректности программы.
    - Программа заканчивается точкой
    - Все определения функция заканчиваются на `;`
    - Все конструкции `if-else-endif` полные
    - Все конструкции `begin-while-repeat` полные
3. Генерация машинного кода.

Правила генерации машинного кода:
- Каждое слово транслируется в инструкцию
- Сложные инструкции (опеределение функций, условные переходы, циклы) содержат инфорацию о размере
- Определение функции хранит свой идентификатор, вызов функции транслируется в вызов по идентификатору

## Модель процессора
Реализовано в модуле: [machine](./machine.py).

Интерфейс командной строки: `python translator.py <input_file> <input_tokens>`

![](./model.jpg)

### DataPath

Реализован в классе `DataPath`.

- `data_memory` -- однопортовая, поэтому либо читаем, либо пишем.

Сигналы (обрабатываются за один такт, реализованы в виде методов класса):

- `latch_data_addr` -- защёлкнуть выбранное значение в data_addr;
- `latch_stack_buffer` -- защёлкнуть в буффер стека значение;
- `latch_increase_(back_)stack_pointer` --  инкрементировать значение `SP` (`CSP`)
- `push` -- записать в стек значение из `stack_buffer` и инкрементировать стек
- `pop` -- снять одно значение со стека в `stack_buffer`
- `save_context` -- записать в стек контекстов значение
- `restore_context` -- снять со стека контекстов значение
- `read` -- прочитать значение из памяти по `data_address` (если `data_address` попадает в память, размапленную на `io`, происходит ввод данных)
- `write` -- записать значение в памяти по `data_address` (если `data_address` попадает в память, размапленную на `io`, происходит вывод данных)


Флаги:
- `zero` -- отражает наличие нулевого значения в аккумуляторе.

## Control Unit
- Hardwired (реализовано полностью на python).
- Моделирование на уровне инструкций.
- Трансляция инструкции в последовательность сигналов: `decode_and_execute_instruction`.

Сигнал:

- `latch_program_couter` -- сигнал для обновления счётчика команд в `ControlUnit`.

Особенности работы модели:

- Для журнала состояний процессора используется стандартный модуль `logging`.
- Остановка моделирования осуществляется при помощи исключений:
    - `EOFError` -- если нет данных для чтения из порта ввода-вывода;
    - `StopIteration` -- если выполнена инструкция halt.
- При исполнении команды `KEY` взводится сигнал `io`
- `Interrupt Handler`:
    - В конце каждой инструкции вызывается функция `interrupt`, в которого происходит проверка и обработка прерываний
    - При обработке прерываний, последующие прерывания запрещаются, идет переключение на пользовательскую функцию, которая обеспечивает обработку

Управление симуляцией реализовано в функции `simultate`.


## Апробация

Пример работы `hello world`:

```bash
[ ca-lab3]$ cat tests/hello_program 
"H" CR
"e" CR
"l" CR
"l" CR
"o" CR
"," CR
"W" CR
"o" CR
"r" CR
"l" CR
"d" CR
"!" CR .


[ ca-lab3]$ python translator.py tests/hello_program tests/hello.f 
source LoC: 25 code instr: 25
[ ca-lab3]$ cat tests/hello.f 
[
    {
        "opcode": "word",
        "type": "quoted",
        "value": "H"
    },
    {
        "opcode": "CR"
    },
    {
        "opcode": "word",
        "type": "quoted",
        "value": "e"
    },
    {
        "opcode": "CR"
    },
    {
        "opcode": "word",
        "type": "quoted",
        "value": "l"
    },
    {
        "opcode": "CR"
    },
    {
        "opcode": "word",
        "type": "quoted",
        "value": "l"
    },
    {
        "opcode": "CR"
    },
    {
        "opcode": "word",
        "type": "quoted",
        "value": "o"
    },
    {
        "opcode": "CR"
    },
    {
        "opcode": "word",
        "type": "quoted",
        "value": ","
    },
    {
        "opcode": "CR"
    },
    {
        "opcode": "word",
        "type": "quoted",
        "value": "W"
    },
    {
        "opcode": "CR"
    },
    {
        "opcode": "word",
        "type": "quoted",
        "value": "o"
    },
    {
        "opcode": "CR"
    },
    {
        "opcode": "word",
        "type": "quoted",
        "value": "r"
    },
    {
        "opcode": "CR"
    },
    {
        "opcode": "word",
        "type": "quoted",
        "value": "l"
    },
    {
        "opcode": "CR"
    },
    {
        "opcode": "word",
        "type": "quoted",
        "value": "d"
    },
    {
        "opcode": "CR"
    },
    {
        "opcode": "word",
        "type": "quoted",
        "value": "!"
    },
    {
        "opcode": "CR"
    },
    {
        "opcode": "."
    }
][ ca-lab3]$ python machine.py tests/hello.f tests/input_file 
[{'opcode': 'word', 'type': 'quoted', 'value': 'H'}, {'opcode': 'CR'}, {'opcode': 'word', 'type': 'quoted', 'value': 'e'}, {'opcode': 'CR'}, {'opcode': 'word', 'type': 'quoted', 'value': 'l'}, {'opcode': 'CR'}, {'opcode': 'word', 'type': 'quoted', 'value': 'l'}, {'opcode': 'CR'}, {'opcode': 'word', 'type': 'quoted', 'value': 'o'}, {'opcode': 'CR'}, {'opcode': 'word', 'type': 'quoted', 'value': ','}, {'opcode': 'CR'}, {'opcode': 'word', 'type': 'quoted', 'value': 'W'}, {'opcode': 'CR'}, {'opcode': 'word', 'type': 'quoted', 'value': 'o'}, {'opcode': 'CR'}, {'opcode': 'word', 'type': 'quoted', 'value': 'r'}, {'opcode': 'CR'}, {'opcode': 'word', 'type': 'quoted', 'value': 'l'}, {'opcode': 'CR'}, {'opcode': 'word', 'type': 'quoted', 'value': 'd'}, {'opcode': 'CR'}, {'opcode': 'word', 'type': 'quoted', 'value': '!'}, {'opcode': 'CR'}, {'opcode': '.'}]
DEBUG:root:{instr: word, value: H} {TICK: 0, PC: 0, ADDR: 0, OUT: 72, SB: 0, SP: -1, STACK: []}
DEBUG:root:{instr: CR, value: } {TICK: 2, PC: 1, ADDR: 0, OUT: 72, SB: 72, SP: 0, STACK: [72]}
output: H
DEBUG:root:{instr: word, value: e} {TICK: 3, PC: 2, ADDR: 128, OUT: 72, SB: 72, SP: -1, STACK: []}
DEBUG:root:{instr: CR, value: } {TICK: 5, PC: 3, ADDR: 128, OUT: 72, SB: 101, SP: 0, STACK: [101]}
output: e
DEBUG:root:{instr: word, value: l} {TICK: 6, PC: 4, ADDR: 128, OUT: 72, SB: 101, SP: -1, STACK: []}
DEBUG:root:{instr: CR, value: } {TICK: 8, PC: 5, ADDR: 128, OUT: 72, SB: 108, SP: 0, STACK: [108]}
output: l
DEBUG:root:{instr: word, value: l} {TICK: 9, PC: 6, ADDR: 128, OUT: 72, SB: 108, SP: -1, STACK: []}
DEBUG:root:{instr: CR, value: } {TICK: 11, PC: 7, ADDR: 128, OUT: 72, SB: 108, SP: 0, STACK: [108]}
output: l
DEBUG:root:{instr: word, value: o} {TICK: 12, PC: 8, ADDR: 128, OUT: 72, SB: 108, SP: -1, STACK: []}
DEBUG:root:{instr: CR, value: } {TICK: 14, PC: 9, ADDR: 128, OUT: 72, SB: 111, SP: 0, STACK: [111]}
output: o
DEBUG:root:{instr: word, value: ,} {TICK: 15, PC: 10, ADDR: 128, OUT: 72, SB: 111, SP: -1, STACK: []}
DEBUG:root:{instr: CR, value: } {TICK: 17, PC: 11, ADDR: 128, OUT: 72, SB: 44, SP: 0, STACK: [44]}
output: ,
DEBUG:root:{instr: word, value: W} {TICK: 18, PC: 12, ADDR: 128, OUT: 72, SB: 44, SP: -1, STACK: []}
DEBUG:root:{instr: CR, value: } {TICK: 20, PC: 13, ADDR: 128, OUT: 72, SB: 87, SP: 0, STACK: [87]}
output: W
DEBUG:root:{instr: word, value: o} {TICK: 21, PC: 14, ADDR: 128, OUT: 72, SB: 87, SP: -1, STACK: []}
DEBUG:root:{instr: CR, value: } {TICK: 23, PC: 15, ADDR: 128, OUT: 72, SB: 111, SP: 0, STACK: [111]}
output: o
DEBUG:root:{instr: word, value: r} {TICK: 24, PC: 16, ADDR: 128, OUT: 72, SB: 111, SP: -1, STACK: []}
DEBUG:root:{instr: CR, value: } {TICK: 26, PC: 17, ADDR: 128, OUT: 72, SB: 114, SP: 0, STACK: [114]}
output: r
DEBUG:root:{instr: word, value: l} {TICK: 27, PC: 18, ADDR: 128, OUT: 72, SB: 114, SP: -1, STACK: []}
DEBUG:root:{instr: CR, value: } {TICK: 29, PC: 19, ADDR: 128, OUT: 72, SB: 108, SP: 0, STACK: [108]}
output: l
DEBUG:root:{instr: word, value: d} {TICK: 30, PC: 20, ADDR: 128, OUT: 72, SB: 108, SP: -1, STACK: []}
DEBUG:root:{instr: CR, value: } {TICK: 32, PC: 21, ADDR: 128, OUT: 72, SB: 100, SP: 0, STACK: [100]}
output: d
DEBUG:root:{instr: word, value: !} {TICK: 33, PC: 22, ADDR: 128, OUT: 72, SB: 100, SP: -1, STACK: []}
DEBUG:root:{instr: CR, value: } {TICK: 35, PC: 23, ADDR: 128, OUT: 72, SB: 33, SP: 0, STACK: [33]}
output: !
DEBUG:root:{instr: ., value: } {TICK: 36, PC: 24, ADDR: 128, OUT: 72, SB: 33, SP: -1, STACK: []}
INFO:root:output_buffer: '[72, 101, 108, 108, 111, 44, 87, 111, 114, 108, 100, 33, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]'
instr_counter:  24 ticks: 36
[ ca-lab3]$
```

Пример работы `cat`:

```bash
[ ca-lab3]$ cat tests/cat_program 
: read KEY CR ;

: read_all
begin
1
while
1
drop
repeat
;

1 read_all .


[ ca-lab3]$ python translator.py tests/cat_program tests/cat.f
source LoC: 17 code instr: 15
[ ca-lab3]$ cat tests/cat.f
[
    {
        "opcode": "function",
        "name": 0,
        "body-size": 3
    },
    {
        "opcode": "KEY"
    },
    {
        "opcode": "CR"
    },
    {
        "opcode": ";"
    },
    {
        "opcode": "function",
        "name": 2,
        "body-size": 7
    },
    {
        "opcode": "begin",
        "begin-size": 2,
        "while-size": 3
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "1"
    },
    {
        "opcode": "while"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "1"
    },
    {
        "opcode": "drop"
    },
    {
        "opcode": "repeat"
    },
    {
        "opcode": ";"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "1"
    },
    {
        "opcode": "word",
        "type": "func",
        "value": 2
    },
    {
        "opcode": "."
    }
]
[ ca-lab3]$ python machine.py tests/cat.f tests/input_file 
[{'opcode': 'function', 'name': 0, 'body-size': 3}, {'opcode': 'KEY'}, {'opcode': 'CR'}, {'opcode': ';'}, {'opcode': 'function', 'name': 2, 'body-size': 7}, {'opcode': 'begin', 'begin-size': 2, 'while-size': 3}, {'opcode': 'word', 'type': 'int', 'value': '1'}, {'opcode': 'while'}, {'opcode': 'word', 'type': 'int', 'value': '1'}, {'opcode': 'drop'}, {'opcode': 'repeat'}, {'opcode': ';'}, {'opcode': 'word', 'type': 'int', 'value': '1'}, {'opcode': 'word', 'type': 'func', 'value': 2}, {'opcode': '.'}]
['5', 'H', '10', 'e', '20', 'l', '25', 'l', '100', 'o']
[5, 10, 20, 25, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
DEBUG:root:{instr: function, value: } {INT: 0, TICK: 0, PC: 0, ADDR: 0, OUT: 72, SB: 0, SP: -1, STACK: []}
DEBUG:root:{instr: function, value: } {INT: 0, TICK: 1, PC: 4, ADDR: 0, OUT: 72, SB: 0, SP: -1, STACK: []}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 2, PC: 12, ADDR: 0, OUT: 72, SB: 0, SP: -1, STACK: []}
DEBUG:root:{instr: word, value: 2} {INT: 0, TICK: 3, PC: 13, ADDR: 0, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: begin, value: } {INT: 0, TICK: 4, PC: 5, ADDR: 0, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: KEY, value: } {INT: 1, TICK: 5, PC: 1, ADDR: 0, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: CR, value: } {INT: 1, TICK: 6, PC: 2, ADDR: 0, OUT: 72, SB: 72, SP: 1, STACK: [1, 72]}
output: H
DEBUG:root:{instr: ;, value: } {INT: 1, TICK: 7, PC: 3, ADDR: 128, OUT: 72, SB: 72, SP: 0, STACK: [1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 8, PC: 6, ADDR: 128, OUT: 72, SB: 72, SP: 0, STACK: [1]}
DEBUG:root:{instr: while, value: } {INT: 0, TICK: 9, PC: 7, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: KEY, value: } {INT: 1, TICK: 10, PC: 1, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: CR, value: } {INT: 1, TICK: 11, PC: 2, ADDR: 0, OUT: 72, SB: 101, SP: 1, STACK: [1, 101]}
output: e
DEBUG:root:{instr: ;, value: } {INT: 1, TICK: 12, PC: 3, ADDR: 128, OUT: 72, SB: 101, SP: 0, STACK: [1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 13, PC: 8, ADDR: 128, OUT: 72, SB: 101, SP: 0, STACK: [1]}
DEBUG:root:{instr: drop, value: } {INT: 0, TICK: 14, PC: 9, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: repeat, value: } {INT: 0, TICK: 15, PC: 10, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 16, PC: 6, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: while, value: } {INT: 0, TICK: 17, PC: 7, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 18, PC: 8, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: drop, value: } {INT: 0, TICK: 19, PC: 9, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: KEY, value: } {INT: 1, TICK: 20, PC: 1, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: CR, value: } {INT: 1, TICK: 21, PC: 2, ADDR: 0, OUT: 72, SB: 108, SP: 1, STACK: [1, 108]}
output: l
DEBUG:root:{instr: ;, value: } {INT: 1, TICK: 22, PC: 3, ADDR: 128, OUT: 72, SB: 108, SP: 0, STACK: [1]}
DEBUG:root:{instr: repeat, value: } {INT: 0, TICK: 23, PC: 10, ADDR: 128, OUT: 72, SB: 108, SP: 0, STACK: [1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 24, PC: 6, ADDR: 128, OUT: 72, SB: 108, SP: 0, STACK: [1]}
DEBUG:root:{instr: KEY, value: } {INT: 1, TICK: 25, PC: 1, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: CR, value: } {INT: 1, TICK: 26, PC: 2, ADDR: 0, OUT: 72, SB: 108, SP: 2, STACK: [1, 1, 108]}
output: l
DEBUG:root:{instr: ;, value: } {INT: 1, TICK: 27, PC: 3, ADDR: 128, OUT: 72, SB: 108, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: while, value: } {INT: 0, TICK: 28, PC: 7, ADDR: 128, OUT: 72, SB: 108, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 29, PC: 8, ADDR: 128, OUT: 72, SB: 108, SP: 0, STACK: [1]}
DEBUG:root:{instr: drop, value: } {INT: 0, TICK: 30, PC: 9, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: repeat, value: } {INT: 0, TICK: 31, PC: 10, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 32, PC: 6, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: while, value: } {INT: 0, TICK: 33, PC: 7, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 34, PC: 8, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: drop, value: } {INT: 0, TICK: 35, PC: 9, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: repeat, value: } {INT: 0, TICK: 36, PC: 10, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 37, PC: 6, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: while, value: } {INT: 0, TICK: 38, PC: 7, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 39, PC: 8, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: drop, value: } {INT: 0, TICK: 40, PC: 9, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: repeat, value: } {INT: 0, TICK: 41, PC: 10, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 42, PC: 6, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: while, value: } {INT: 0, TICK: 43, PC: 7, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 44, PC: 8, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: drop, value: } {INT: 0, TICK: 45, PC: 9, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: repeat, value: } {INT: 0, TICK: 46, PC: 10, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 47, PC: 6, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: while, value: } {INT: 0, TICK: 48, PC: 7, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 49, PC: 8, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: drop, value: } {INT: 0, TICK: 50, PC: 9, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: repeat, value: } {INT: 0, TICK: 51, PC: 10, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 52, PC: 6, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: while, value: } {INT: 0, TICK: 53, PC: 7, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 54, PC: 8, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: drop, value: } {INT: 0, TICK: 55, PC: 9, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: repeat, value: } {INT: 0, TICK: 56, PC: 10, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 57, PC: 6, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: while, value: } {INT: 0, TICK: 58, PC: 7, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 59, PC: 8, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: drop, value: } {INT: 0, TICK: 60, PC: 9, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: repeat, value: } {INT: 0, TICK: 61, PC: 10, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 62, PC: 6, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: while, value: } {INT: 0, TICK: 63, PC: 7, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 64, PC: 8, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: drop, value: } {INT: 0, TICK: 65, PC: 9, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: repeat, value: } {INT: 0, TICK: 66, PC: 10, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 67, PC: 6, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: while, value: } {INT: 0, TICK: 68, PC: 7, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 69, PC: 8, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: drop, value: } {INT: 0, TICK: 70, PC: 9, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: repeat, value: } {INT: 0, TICK: 71, PC: 10, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 72, PC: 6, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: while, value: } {INT: 0, TICK: 73, PC: 7, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 74, PC: 8, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: drop, value: } {INT: 0, TICK: 75, PC: 9, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: repeat, value: } {INT: 0, TICK: 76, PC: 10, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 77, PC: 6, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: while, value: } {INT: 0, TICK: 78, PC: 7, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 79, PC: 8, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: drop, value: } {INT: 0, TICK: 80, PC: 9, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: repeat, value: } {INT: 0, TICK: 81, PC: 10, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 82, PC: 6, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: while, value: } {INT: 0, TICK: 83, PC: 7, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 84, PC: 8, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: drop, value: } {INT: 0, TICK: 85, PC: 9, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: repeat, value: } {INT: 0, TICK: 86, PC: 10, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 87, PC: 6, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: while, value: } {INT: 0, TICK: 88, PC: 7, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 89, PC: 8, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: drop, value: } {INT: 0, TICK: 90, PC: 9, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: repeat, value: } {INT: 0, TICK: 91, PC: 10, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 92, PC: 6, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: while, value: } {INT: 0, TICK: 93, PC: 7, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 94, PC: 8, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: drop, value: } {INT: 0, TICK: 95, PC: 9, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: repeat, value: } {INT: 0, TICK: 96, PC: 10, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 97, PC: 6, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: while, value: } {INT: 0, TICK: 98, PC: 7, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: word, value: 1} {INT: 0, TICK: 99, PC: 8, ADDR: 128, OUT: 72, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: KEY, value: } {INT: 1, TICK: 100, PC: 1, ADDR: 128, OUT: 72, SB: 1, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: CR, value: } {INT: 1, TICK: 101, PC: 2, ADDR: 0, OUT: 72, SB: 111, SP: 2, STACK: [1, 1, 111]}
output: o
DEBUG:root:{instr: ;, value: } {INT: 1, TICK: 102, PC: 3, ADDR: 128, OUT: 72, SB: 111, SP: 1, STACK: [1, 1]}
DEBUG:root:{instr: KEY, value: } {INT: 1, TICK: 103, PC: 1, ADDR: 128, OUT: 72, SB: 111, SP: 1, STACK: [1, 1]}
INFO:root:output_buffer: '[72, 101, 108, 108, 111, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]'
[72, 101, 108, 108, 111, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
instr_counter:  103 ticks: 104
```

Как мы видим, чтение каждого символа происходит только в тот момент, когда значение `PC` достигает указанного в расписании значения 

Пример работы на программе `Euler problem 5` (для чисел от 1 до 10 для разумного вывода)


```bash
[ ca-lab3]$ cat tests/prob5_program 
: gcd begin dup while tuck mod repeat drop ;
: lcm over over * -rot gcd / ;

: prob5
over 1 = IF
    *
else
    lcm prob5
endif ;

1 2 3 4 5 6 7 8 9 10 prob5 .
[ ca-lab3]$ python translator.py tests/prob5_program tests/prob5.f 
source LoC: 43 code instr: 40
[ ca-lab3]$ cat tests/prob5.f 
[
    {
        "opcode": "function",
        "name": 0,
        "body-size": 8
    },
    {
        "opcode": "begin",
        "begin-size": 2,
        "while-size": 3
    },
    {
        "opcode": "dup"
    },
    {
        "opcode": "while"
    },
    {
        "opcode": "tuck"
    },
    {
        "opcode": "mod"
    },
    {
        "opcode": "repeat"
    },
    {
        "opcode": "drop"
    },
    {
        "opcode": ";"
    },
    {
        "opcode": "function",
        "name": 1,
        "body-size": 7
    },
    {
        "opcode": "over"
    },
    {
        "opcode": "over"
    },
    {
        "opcode": "*"
    },
    {
        "opcode": "-rot"
    },
    {
        "opcode": "word",
        "type": "func",
        "value": 0
    },
    {
        "opcode": "/"
    },
    {
        "opcode": ";"
    },
    {
        "opcode": "function",
        "name": 2,
        "body-size": 10
    },
    {
        "opcode": "over"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "1"
    },
    {
        "opcode": "="
    },
    {
        "opcode": "if",
        "true-size": 2,
        "false-size": 3
    },
    {
        "opcode": "*"
    },
    {
        "opcode": ";"
    },
    {
        "opcode": "word",
        "type": "func",
        "value": 1
    },
    {
        "opcode": "word",
        "type": "func",
        "value": 2
    },
    {
        "opcode": ";"
    },
    {
        "opcode": ";"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "1"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "2"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "3"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "4"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "5"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "6"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "7"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "8"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "9"
    },
    {
        "opcode": "word",
        "type": "int",
        "value": "10"
    },
    {
        "opcode": "word",
        "type": "func",
        "value": 2
    },
    {
        "opcode": "."
    }
][ ca-lab3]$python ![model.jpg](..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2Fhome%2Faidar%2F%D0%97%D0%B0%D0%B3%D1%80%D1%83%D0%B7%D0%BA%D0%B8%2FTelegram%20Desktop%2Fca-lab3%2Fmodel.jpg)machine.py tests/prob5.f tests/input_file
[{'opcode': 'function', 'name': 0, 'body-size': 8}, {'opcode': 'begin', 'begin-size': 2, 'while-size': 3}, {'opcode': 'dup'}, {'opcode': 'while'}, {'opcode': 'tuck'}, {'opcode': 'mod'}, {'opcode': 'repeat'}, {'opcode': 'drop'}, {'opcode': ';'}, {'opcode': 'function', 'name': 1, 'body-size': 7}, {'opcode': 'over'}, {'opcode': 'over'}, {'opcode': '*'}, {'opcode': '-rot'}, {'opcode': 'word', 'type': 'func', 'value': 0}, {'opcode': '/'}, {'opcode': ';'}, {'opcode': 'function', 'name': 2, 'body-size': 10}, {'opcode': 'over'}, {'opcode': 'word', 'type': 'int', 'value': '1'}, {'opcode': '='}, {'opcode': 'if', 'true-size': 2, 'false-size': 3}, {'opcode': '*'}, {'opcode': ';'}, {'opcode': 'word', 'type': 'func', 'value': 1}, {'opcode': 'word', 'type': 'func', 'value': 2}, {'opcode': ';'}, {'opcode': ';'}, {'opcode': 'word', 'type': 'int', 'value': '1'}, {'opcode': 'word', 'type': 'int', 'value': '2'}, {'opcode': 'word', 'type': 'int', 'value': '3'}, {'opcode': 'word', 'type': 'int', 'value': '4'}, {'opcode': 'word', 'type': 'int', 'value': '5'}, {'opcode': 'word', 'type': 'int', 'value': '6'}, {'opcode': 'word', 'type': 'int', 'value': '7'}, {'opcode': 'word', 'type': 'int', 'value': '8'}, {'opcode': 'word', 'type': 'int', 'value': '9'}, {'opcode': 'word', 'type': 'int', 'value': '10'}, {'opcode': 'word', 'type': 'func', 'value': 2}, {'opcode': '.'}]
DEBUG:root:{instr: function, value: } {TICK: 0, PC: 0, ADDR: 0, OUT: 78, SB: 0, SP: -1, STACK: []}
DEBUG:root:{instr: function, value: } {TICK: 1, PC: 9, ADDR: 0, OUT: 78, SB: 0, SP: -1, STACK: []}
DEBUG:root:{instr: function, value: } {TICK: 2, PC: 17, ADDR: 0, OUT: 78, SB: 0, SP: -1, STACK: []}
DEBUG:root:{instr: word, value: 1} {TICK: 3, PC: 28, ADDR: 0, OUT: 78, SB: 0, SP: -1, STACK: []}
DEBUG:root:{instr: word, value: 2} {TICK: 5, PC: 29, ADDR: 0, OUT: 78, SB: 1, SP: 0, STACK: [1]}
DEBUG:root:{instr: word, value: 3} {TICK: 7, PC: 30, ADDR: 0, OUT: 78, SB: 2, SP: 1, STACK: [1, 2]}
DEBUG:root:{instr: word, value: 4} {TICK: 9, PC: 31, ADDR: 0, OUT: 78, SB: 3, SP: 2, STACK: [1, 2, 3]}
DEBUG:root:{instr: word, value: 5} {TICK: 11, PC: 32, ADDR: 0, OUT: 78, SB: 4, SP: 3, STACK: [1, 2, 3, 4]}
DEBUG:root:{instr: word, value: 6} {TICK: 13, PC: 33, ADDR: 0, OUT: 78, SB: 5, SP: 4, STACK: [1, 2, 3, 4, 5]}
DEBUG:root:{instr: word, value: 7} {TICK: 15, PC: 34, ADDR: 0, OUT: 78, SB: 6, SP: 5, STACK: [1, 2, 3, 4, 5, 6]}
DEBUG:root:{instr: word, value: 8} {TICK: 17, PC: 35, ADDR: 0, OUT: 78, SB: 7, SP: 6, STACK: [1, 2, 3, 4, 5, 6, 7]}
DEBUG:root:{instr: word, value: 9} {TICK: 19, PC: 36, ADDR: 0, OUT: 78, SB: 8, SP: 7, STACK: [1, 2, 3, 4, 5, 6, 7, 8]}
DEBUG:root:{instr: word, value: 10} {TICK: 21, PC: 37, ADDR: 0, OUT: 78, SB: 9, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 9]}
DEBUG:root:{instr: word, value: 2} {TICK: 23, PC: 38, ADDR: 0, OUT: 78, SB: 10, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
DEBUG:root:{instr: over, value: } {TICK: 25, PC: 18, ADDR: 0, OUT: 78, SB: 10, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
DEBUG:root:{instr: word, value: 1} {TICK: 27, PC: 19, ADDR: 0, OUT: 78, SB: 9, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 9]}
DEBUG:root:{instr: =, value: } {TICK: 29, PC: 20, ADDR: 0, OUT: 78, SB: 1, SP: 11, STACK: [2, 3, 4, 5, 6, 7, 8, 9, 10, 9, 1]}
DEBUG:root:{instr: if, value: } {TICK: 33, PC: 21, ADDR: 0, OUT: 78, SB: 0, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 0]}
DEBUG:root:{instr: word, value: 1} {TICK: 35, PC: 24, ADDR: 0, OUT: 78, SB: 0, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
DEBUG:root:{instr: over, value: } {TICK: 37, PC: 10, ADDR: 0, OUT: 78, SB: 0, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
DEBUG:root:{instr: over, value: } {TICK: 39, PC: 11, ADDR: 0, OUT: 78, SB: 9, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 9]}
DEBUG:root:{instr: *, value: } {TICK: 41, PC: 12, ADDR: 0, OUT: 78, SB: 10, SP: 11, STACK: [2, 3, 4, 5, 6, 7, 8, 9, 10, 9, 10]}
DEBUG:root:{instr: -rot, value: } {TICK: 45, PC: 13, ADDR: 0, OUT: 78, SB: 90, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 90]}
DEBUG:root:{instr: word, value: 0} {TICK: 54, PC: 14, ADDR: 0, OUT: 78, SB: 10, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90, 9, 10]}
DEBUG:root:{instr: begin, value: } {TICK: 56, PC: 1, ADDR: 0, OUT: 78, SB: 10, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90, 9, 10]}
DEBUG:root:{instr: dup, value: } {TICK: 58, PC: 2, ADDR: 0, OUT: 78, SB: 10, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90, 9, 10]}
DEBUG:root:{instr: while, value: } {TICK: 60, PC: 3, ADDR: 0, OUT: 78, SB: 10, SP: 11, STACK: [2, 3, 4, 5, 6, 7, 8, 90, 9, 10, 10]}
DEBUG:root:{instr: tuck, value: } {TICK: 62, PC: 4, ADDR: 0, OUT: 78, SB: 10, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90, 9, 10]}
DEBUG:root:{instr: mod, value: } {TICK: 70, PC: 5, ADDR: 0, OUT: 78, SB: 10, SP: 11, STACK: [2, 3, 4, 5, 6, 7, 8, 90, 10, 9, 10]}
DEBUG:root:{instr: repeat, value: } {TICK: 74, PC: 6, ADDR: 0, OUT: 78, SB: 9, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90, 10, 9]}
DEBUG:root:{instr: dup, value: } {TICK: 75, PC: 2, ADDR: 0, OUT: 78, SB: 9, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90, 10, 9]}
DEBUG:root:{instr: while, value: } {TICK: 77, PC: 3, ADDR: 0, OUT: 78, SB: 9, SP: 11, STACK: [2, 3, 4, 5, 6, 7, 8, 90, 10, 9, 9]}
DEBUG:root:{instr: tuck, value: } {TICK: 79, PC: 4, ADDR: 0, OUT: 78, SB: 9, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90, 10, 9]}
DEBUG:root:{instr: mod, value: } {TICK: 87, PC: 5, ADDR: 0, OUT: 78, SB: 9, SP: 11, STACK: [2, 3, 4, 5, 6, 7, 8, 90, 9, 10, 9]}
DEBUG:root:{instr: repeat, value: } {TICK: 91, PC: 6, ADDR: 0, OUT: 78, SB: 1, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90, 9, 1]}
DEBUG:root:{instr: dup, value: } {TICK: 92, PC: 2, ADDR: 0, OUT: 78, SB: 1, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90, 9, 1]}
DEBUG:root:{instr: while, value: } {TICK: 94, PC: 3, ADDR: 0, OUT: 78, SB: 1, SP: 11, STACK: [2, 3, 4, 5, 6, 7, 8, 90, 9, 1, 1]}
DEBUG:root:{instr: tuck, value: } {TICK: 96, PC: 4, ADDR: 0, OUT: 78, SB: 1, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90, 9, 1]}
DEBUG:root:{instr: mod, value: } {TICK: 104, PC: 5, ADDR: 0, OUT: 78, SB: 1, SP: 11, STACK: [2, 3, 4, 5, 6, 7, 8, 90, 1, 9, 1]}
DEBUG:root:{instr: repeat, value: } {TICK: 108, PC: 6, ADDR: 0, OUT: 78, SB: 0, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90, 1, 0]}
DEBUG:root:{instr: dup, value: } {TICK: 109, PC: 2, ADDR: 0, OUT: 78, SB: 0, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90, 1, 0]}
DEBUG:root:{instr: while, value: } {TICK: 111, PC: 3, ADDR: 0, OUT: 78, SB: 0, SP: 11, STACK: [2, 3, 4, 5, 6, 7, 8, 90, 1, 0, 0]}
DEBUG:root:{instr: drop, value: } {TICK: 114, PC: 7, ADDR: 0, OUT: 78, SB: 0, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90, 1, 0]}
DEBUG:root:{instr: ;, value: } {TICK: 115, PC: 8, ADDR: 0, OUT: 78, SB: 0, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90, 1]}
DEBUG:root:{instr: /, value: } {TICK: 116, PC: 15, ADDR: 0, OUT: 78, SB: 0, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90, 1]}
DEBUG:root:{instr: ;, value: } {TICK: 120, PC: 16, ADDR: 0, OUT: 78, SB: 90, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90]}
DEBUG:root:{instr: word, value: 2} {TICK: 121, PC: 25, ADDR: 0, OUT: 78, SB: 90, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90]}
DEBUG:root:{instr: over, value: } {TICK: 123, PC: 18, ADDR: 0, OUT: 78, SB: 90, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90]}
DEBUG:root:{instr: word, value: 1} {TICK: 125, PC: 19, ADDR: 0, OUT: 78, SB: 8, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90, 8]}
DEBUG:root:{instr: =, value: } {TICK: 127, PC: 20, ADDR: 0, OUT: 78, SB: 1, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90, 8, 1]}
DEBUG:root:{instr: if, value: } {TICK: 131, PC: 21, ADDR: 0, OUT: 78, SB: 0, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90, 0]}
DEBUG:root:{instr: word, value: 1} {TICK: 133, PC: 24, ADDR: 0, OUT: 78, SB: 0, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90]}
DEBUG:root:{instr: over, value: } {TICK: 135, PC: 10, ADDR: 0, OUT: 78, SB: 0, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90]}
DEBUG:root:{instr: over, value: } {TICK: 137, PC: 11, ADDR: 0, OUT: 78, SB: 8, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90, 8]}
DEBUG:root:{instr: *, value: } {TICK: 139, PC: 12, ADDR: 0, OUT: 78, SB: 90, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90, 8, 90]}
DEBUG:root:{instr: -rot, value: } {TICK: 143, PC: 13, ADDR: 0, OUT: 78, SB: 720, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 8, 90, 720]}
DEBUG:root:{instr: word, value: 0} {TICK: 152, PC: 14, ADDR: 0, OUT: 78, SB: 90, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 720, 8, 90]}
DEBUG:root:{instr: begin, value: } {TICK: 154, PC: 1, ADDR: 0, OUT: 78, SB: 90, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 720, 8, 90]}
DEBUG:root:{instr: dup, value: } {TICK: 156, PC: 2, ADDR: 0, OUT: 78, SB: 90, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 720, 8, 90]}
DEBUG:root:{instr: while, value: } {TICK: 158, PC: 3, ADDR: 0, OUT: 78, SB: 90, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 720, 8, 90, 90]}
DEBUG:root:{instr: tuck, value: } {TICK: 160, PC: 4, ADDR: 0, OUT: 78, SB: 90, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 720, 8, 90]}
DEBUG:root:{instr: mod, value: } {TICK: 168, PC: 5, ADDR: 0, OUT: 78, SB: 90, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 720, 90, 8, 90]}
DEBUG:root:{instr: repeat, value: } {TICK: 172, PC: 6, ADDR: 0, OUT: 78, SB: 8, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 720, 90, 8]}
DEBUG:root:{instr: dup, value: } {TICK: 173, PC: 2, ADDR: 0, OUT: 78, SB: 8, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 720, 90, 8]}
DEBUG:root:{instr: while, value: } {TICK: 175, PC: 3, ADDR: 0, OUT: 78, SB: 8, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 720, 90, 8, 8]}
DEBUG:root:{instr: tuck, value: } {TICK: 177, PC: 4, ADDR: 0, OUT: 78, SB: 8, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 720, 90, 8]}
DEBUG:root:{instr: mod, value: } {TICK: 185, PC: 5, ADDR: 0, OUT: 78, SB: 8, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 720, 8, 90, 8]}
DEBUG:root:{instr: repeat, value: } {TICK: 189, PC: 6, ADDR: 0, OUT: 78, SB: 2, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 720, 8, 2]}
DEBUG:root:{instr: dup, value: } {TICK: 190, PC: 2, ADDR: 0, OUT: 78, SB: 2, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 720, 8, 2]}
DEBUG:root:{instr: while, value: } {TICK: 192, PC: 3, ADDR: 0, OUT: 78, SB: 2, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 720, 8, 2, 2]}
DEBUG:root:{instr: tuck, value: } {TICK: 194, PC: 4, ADDR: 0, OUT: 78, SB: 2, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 720, 8, 2]}
DEBUG:root:{instr: mod, value: } {TICK: 202, PC: 5, ADDR: 0, OUT: 78, SB: 2, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 720, 2, 8, 2]}
DEBUG:root:{instr: repeat, value: } {TICK: 206, PC: 6, ADDR: 0, OUT: 78, SB: 0, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 720, 2, 0]}
DEBUG:root:{instr: dup, value: } {TICK: 207, PC: 2, ADDR: 0, OUT: 78, SB: 0, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 720, 2, 0]}
DEBUG:root:{instr: while, value: } {TICK: 209, PC: 3, ADDR: 0, OUT: 78, SB: 0, SP: 10, STACK: [1, 2, 3, 4, 5, 6, 7, 720, 2, 0, 0]}
DEBUG:root:{instr: drop, value: } {TICK: 212, PC: 7, ADDR: 0, OUT: 78, SB: 0, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 720, 2, 0]}
DEBUG:root:{instr: ;, value: } {TICK: 213, PC: 8, ADDR: 0, OUT: 78, SB: 0, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 7, 720, 2]}
DEBUG:root:{instr: /, value: } {TICK: 214, PC: 15, ADDR: 0, OUT: 78, SB: 0, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 7, 720, 2]}
DEBUG:root:{instr: ;, value: } {TICK: 218, PC: 16, ADDR: 0, OUT: 78, SB: 360, SP: 7, STACK: [1, 2, 3, 4, 5, 6, 7, 360]}
DEBUG:root:{instr: word, value: 2} {TICK: 219, PC: 25, ADDR: 0, OUT: 78, SB: 360, SP: 7, STACK: [1, 2, 3, 4, 5, 6, 7, 360]}
DEBUG:root:{instr: over, value: } {TICK: 221, PC: 18, ADDR: 0, OUT: 78, SB: 360, SP: 7, STACK: [1, 2, 3, 4, 5, 6, 7, 360]}
DEBUG:root:{instr: word, value: 1} {TICK: 223, PC: 19, ADDR: 0, OUT: 78, SB: 7, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 7, 360, 7]}
DEBUG:root:{instr: =, value: } {TICK: 225, PC: 20, ADDR: 0, OUT: 78, SB: 1, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 360, 7, 1]}
DEBUG:root:{instr: if, value: } {TICK: 229, PC: 21, ADDR: 0, OUT: 78, SB: 0, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 7, 360, 0]}
DEBUG:root:{instr: word, value: 1} {TICK: 231, PC: 24, ADDR: 0, OUT: 78, SB: 0, SP: 7, STACK: [1, 2, 3, 4, 5, 6, 7, 360]}
DEBUG:root:{instr: over, value: } {TICK: 233, PC: 10, ADDR: 0, OUT: 78, SB: 0, SP: 7, STACK: [1, 2, 3, 4, 5, 6, 7, 360]}
DEBUG:root:{instr: over, value: } {TICK: 235, PC: 11, ADDR: 0, OUT: 78, SB: 7, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 7, 360, 7]}
DEBUG:root:{instr: *, value: } {TICK: 237, PC: 12, ADDR: 0, OUT: 78, SB: 360, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 7, 360, 7, 360]}
DEBUG:root:{instr: -rot, value: } {TICK: 241, PC: 13, ADDR: 0, OUT: 78, SB: 2520, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 7, 360, 2520]}
DEBUG:root:{instr: word, value: 0} {TICK: 250, PC: 14, ADDR: 0, OUT: 78, SB: 360, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 2520, 7, 360]}
DEBUG:root:{instr: begin, value: } {TICK: 252, PC: 1, ADDR: 0, OUT: 78, SB: 360, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 2520, 7, 360]}
DEBUG:root:{instr: dup, value: } {TICK: 254, PC: 2, ADDR: 0, OUT: 78, SB: 360, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 2520, 7, 360]}
DEBUG:root:{instr: while, value: } {TICK: 256, PC: 3, ADDR: 0, OUT: 78, SB: 360, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 2520, 7, 360, 360]}
DEBUG:root:{instr: tuck, value: } {TICK: 258, PC: 4, ADDR: 0, OUT: 78, SB: 360, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 2520, 7, 360]}
DEBUG:root:{instr: mod, value: } {TICK: 266, PC: 5, ADDR: 0, OUT: 78, SB: 360, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 2520, 360, 7, 360]}
DEBUG:root:{instr: repeat, value: } {TICK: 270, PC: 6, ADDR: 0, OUT: 78, SB: 7, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 2520, 360, 7]}
DEBUG:root:{instr: dup, value: } {TICK: 271, PC: 2, ADDR: 0, OUT: 78, SB: 7, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 2520, 360, 7]}
DEBUG:root:{instr: while, value: } {TICK: 273, PC: 3, ADDR: 0, OUT: 78, SB: 7, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 2520, 360, 7, 7]}
DEBUG:root:{instr: tuck, value: } {TICK: 275, PC: 4, ADDR: 0, OUT: 78, SB: 7, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 2520, 360, 7]}
DEBUG:root:{instr: mod, value: } {TICK: 283, PC: 5, ADDR: 0, OUT: 78, SB: 7, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 2520, 7, 360, 7]}
DEBUG:root:{instr: repeat, value: } {TICK: 287, PC: 6, ADDR: 0, OUT: 78, SB: 3, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 2520, 7, 3]}
DEBUG:root:{instr: dup, value: } {TICK: 288, PC: 2, ADDR: 0, OUT: 78, SB: 3, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 2520, 7, 3]}
DEBUG:root:{instr: while, value: } {TICK: 290, PC: 3, ADDR: 0, OUT: 78, SB: 3, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 2520, 7, 3, 3]}
DEBUG:root:{instr: tuck, value: } {TICK: 292, PC: 4, ADDR: 0, OUT: 78, SB: 3, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 2520, 7, 3]}
DEBUG:root:{instr: mod, value: } {TICK: 300, PC: 5, ADDR: 0, OUT: 78, SB: 3, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 2520, 3, 7, 3]}
DEBUG:root:{instr: repeat, value: } {TICK: 304, PC: 6, ADDR: 0, OUT: 78, SB: 1, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 2520, 3, 1]}
DEBUG:root:{instr: dup, value: } {TICK: 305, PC: 2, ADDR: 0, OUT: 78, SB: 1, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 2520, 3, 1]}
DEBUG:root:{instr: while, value: } {TICK: 307, PC: 3, ADDR: 0, OUT: 78, SB: 1, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 2520, 3, 1, 1]}
DEBUG:root:{instr: tuck, value: } {TICK: 309, PC: 4, ADDR: 0, OUT: 78, SB: 1, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 2520, 3, 1]}
DEBUG:root:{instr: mod, value: } {TICK: 317, PC: 5, ADDR: 0, OUT: 78, SB: 1, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 2520, 1, 3, 1]}
DEBUG:root:{instr: repeat, value: } {TICK: 321, PC: 6, ADDR: 0, OUT: 78, SB: 0, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 2520, 1, 0]}
DEBUG:root:{instr: dup, value: } {TICK: 322, PC: 2, ADDR: 0, OUT: 78, SB: 0, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 2520, 1, 0]}
DEBUG:root:{instr: while, value: } {TICK: 324, PC: 3, ADDR: 0, OUT: 78, SB: 0, SP: 9, STACK: [1, 2, 3, 4, 5, 6, 2520, 1, 0, 0]}
DEBUG:root:{instr: drop, value: } {TICK: 327, PC: 7, ADDR: 0, OUT: 78, SB: 0, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 2520, 1, 0]}
DEBUG:root:{instr: ;, value: } {TICK: 328, PC: 8, ADDR: 0, OUT: 78, SB: 0, SP: 7, STACK: [1, 2, 3, 4, 5, 6, 2520, 1]}
DEBUG:root:{instr: /, value: } {TICK: 329, PC: 15, ADDR: 0, OUT: 78, SB: 0, SP: 7, STACK: [1, 2, 3, 4, 5, 6, 2520, 1]}
DEBUG:root:{instr: ;, value: } {TICK: 333, PC: 16, ADDR: 0, OUT: 78, SB: 2520, SP: 6, STACK: [1, 2, 3, 4, 5, 6, 2520]}
DEBUG:root:{instr: word, value: 2} {TICK: 334, PC: 25, ADDR: 0, OUT: 78, SB: 2520, SP: 6, STACK: [1, 2, 3, 4, 5, 6, 2520]}
DEBUG:root:{instr: over, value: } {TICK: 336, PC: 18, ADDR: 0, OUT: 78, SB: 2520, SP: 6, STACK: [1, 2, 3, 4, 5, 6, 2520]}
DEBUG:root:{instr: word, value: 1} {TICK: 338, PC: 19, ADDR: 0, OUT: 78, SB: 6, SP: 7, STACK: [1, 2, 3, 4, 5, 6, 2520, 6]}
DEBUG:root:{instr: =, value: } {TICK: 340, PC: 20, ADDR: 0, OUT: 78, SB: 1, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 2520, 6, 1]}
DEBUG:root:{instr: if, value: } {TICK: 344, PC: 21, ADDR: 0, OUT: 78, SB: 0, SP: 7, STACK: [1, 2, 3, 4, 5, 6, 2520, 0]}
DEBUG:root:{instr: word, value: 1} {TICK: 346, PC: 24, ADDR: 0, OUT: 78, SB: 0, SP: 6, STACK: [1, 2, 3, 4, 5, 6, 2520]}
DEBUG:root:{instr: over, value: } {TICK: 348, PC: 10, ADDR: 0, OUT: 78, SB: 0, SP: 6, STACK: [1, 2, 3, 4, 5, 6, 2520]}
DEBUG:root:{instr: over, value: } {TICK: 350, PC: 11, ADDR: 0, OUT: 78, SB: 6, SP: 7, STACK: [1, 2, 3, 4, 5, 6, 2520, 6]}
DEBUG:root:{instr: *, value: } {TICK: 352, PC: 12, ADDR: 0, OUT: 78, SB: 2520, SP: 8, STACK: [1, 2, 3, 4, 5, 6, 2520, 6, 2520]}
DEBUG:root:{instr: -rot, value: } {TICK: 356, PC: 13, ADDR: 0, OUT: 78, SB: 15120, SP: 7, STACK: [1, 2, 3, 4, 5, 6, 2520, 15120]}
DEBUG:root:{instr: word, value: 0} {TICK: 365, PC: 14, ADDR: 0, OUT: 78, SB: 2520, SP: 7, STACK: [1, 2, 3, 4, 5, 15120, 6, 2520]}
DEBUG:root:{instr: begin, value: } {TICK: 367, PC: 1, ADDR: 0, OUT: 78, SB: 2520, SP: 7, STACK: [1, 2, 3, 4, 5, 15120, 6, 2520]}
DEBUG:root:{instr: dup, value: } {TICK: 369, PC: 2, ADDR: 0, OUT: 78, SB: 2520, SP: 7, STACK: [1, 2, 3, 4, 5, 15120, 6, 2520]}
DEBUG:root:{instr: while, value: } {TICK: 371, PC: 3, ADDR: 0, OUT: 78, SB: 2520, SP: 8, STACK: [1, 2, 3, 4, 5, 15120, 6, 2520, 2520]}
DEBUG:root:{instr: tuck, value: } {TICK: 373, PC: 4, ADDR: 0, OUT: 78, SB: 2520, SP: 7, STACK: [1, 2, 3, 4, 5, 15120, 6, 2520]}
DEBUG:root:{instr: mod, value: } {TICK: 381, PC: 5, ADDR: 0, OUT: 78, SB: 2520, SP: 8, STACK: [1, 2, 3, 4, 5, 15120, 2520, 6, 2520]}
DEBUG:root:{instr: repeat, value: } {TICK: 385, PC: 6, ADDR: 0, OUT: 78, SB: 6, SP: 7, STACK: [1, 2, 3, 4, 5, 15120, 2520, 6]}
DEBUG:root:{instr: dup, value: } {TICK: 386, PC: 2, ADDR: 0, OUT: 78, SB: 6, SP: 7, STACK: [1, 2, 3, 4, 5, 15120, 2520, 6]}
DEBUG:root:{instr: while, value: } {TICK: 388, PC: 3, ADDR: 0, OUT: 78, SB: 6, SP: 8, STACK: [1, 2, 3, 4, 5, 15120, 2520, 6, 6]}
DEBUG:root:{instr: tuck, value: } {TICK: 390, PC: 4, ADDR: 0, OUT: 78, SB: 6, SP: 7, STACK: [1, 2, 3, 4, 5, 15120, 2520, 6]}
DEBUG:root:{instr: mod, value: } {TICK: 398, PC: 5, ADDR: 0, OUT: 78, SB: 6, SP: 8, STACK: [1, 2, 3, 4, 5, 15120, 6, 2520, 6]}
DEBUG:root:{instr: repeat, value: } {TICK: 402, PC: 6, ADDR: 0, OUT: 78, SB: 0, SP: 7, STACK: [1, 2, 3, 4, 5, 15120, 6, 0]}
DEBUG:root:{instr: dup, value: } {TICK: 403, PC: 2, ADDR: 0, OUT: 78, SB: 0, SP: 7, STACK: [1, 2, 3, 4, 5, 15120, 6, 0]}
DEBUG:root:{instr: while, value: } {TICK: 405, PC: 3, ADDR: 0, OUT: 78, SB: 0, SP: 8, STACK: [1, 2, 3, 4, 5, 15120, 6, 0, 0]}
DEBUG:root:{instr: drop, value: } {TICK: 408, PC: 7, ADDR: 0, OUT: 78, SB: 0, SP: 7, STACK: [1, 2, 3, 4, 5, 15120, 6, 0]}
DEBUG:root:{instr: ;, value: } {TICK: 409, PC: 8, ADDR: 0, OUT: 78, SB: 0, SP: 6, STACK: [1, 2, 3, 4, 5, 15120, 6]}
DEBUG:root:{instr: /, value: } {TICK: 410, PC: 15, ADDR: 0, OUT: 78, SB: 0, SP: 6, STACK: [1, 2, 3, 4, 5, 15120, 6]}
DEBUG:root:{instr: ;, value: } {TICK: 414, PC: 16, ADDR: 0, OUT: 78, SB: 2520, SP: 5, STACK: [1, 2, 3, 4, 5, 2520]}
DEBUG:root:{instr: word, value: 2} {TICK: 415, PC: 25, ADDR: 0, OUT: 78, SB: 2520, SP: 5, STACK: [1, 2, 3, 4, 5, 2520]}
DEBUG:root:{instr: over, value: } {TICK: 417, PC: 18, ADDR: 0, OUT: 78, SB: 2520, SP: 5, STACK: [1, 2, 3, 4, 5, 2520]}
DEBUG:root:{instr: word, value: 1} {TICK: 419, PC: 19, ADDR: 0, OUT: 78, SB: 5, SP: 6, STACK: [1, 2, 3, 4, 5, 2520, 5]}
DEBUG:root:{instr: =, value: } {TICK: 421, PC: 20, ADDR: 0, OUT: 78, SB: 1, SP: 7, STACK: [1, 2, 3, 4, 5, 2520, 5, 1]}
DEBUG:root:{instr: if, value: } {TICK: 425, PC: 21, ADDR: 0, OUT: 78, SB: 0, SP: 6, STACK: [1, 2, 3, 4, 5, 2520, 0]}
DEBUG:root:{instr: word, value: 1} {TICK: 427, PC: 24, ADDR: 0, OUT: 78, SB: 0, SP: 5, STACK: [1, 2, 3, 4, 5, 2520]}
DEBUG:root:{instr: over, value: } {TICK: 429, PC: 10, ADDR: 0, OUT: 78, SB: 0, SP: 5, STACK: [1, 2, 3, 4, 5, 2520]}
DEBUG:root:{instr: over, value: } {TICK: 431, PC: 11, ADDR: 0, OUT: 78, SB: 5, SP: 6, STACK: [1, 2, 3, 4, 5, 2520, 5]}
DEBUG:root:{instr: *, value: } {TICK: 433, PC: 12, ADDR: 0, OUT: 78, SB: 2520, SP: 7, STACK: [1, 2, 3, 4, 5, 2520, 5, 2520]}
DEBUG:root:{instr: -rot, value: } {TICK: 437, PC: 13, ADDR: 0, OUT: 78, SB: 12600, SP: 6, STACK: [1, 2, 3, 4, 5, 2520, 12600]}
DEBUG:root:{instr: word, value: 0} {TICK: 446, PC: 14, ADDR: 0, OUT: 78, SB: 2520, SP: 6, STACK: [1, 2, 3, 4, 12600, 5, 2520]}
DEBUG:root:{instr: begin, value: } {TICK: 448, PC: 1, ADDR: 0, OUT: 78, SB: 2520, SP: 6, STACK: [1, 2, 3, 4, 12600, 5, 2520]}
DEBUG:root:{instr: dup, value: } {TICK: 450, PC: 2, ADDR: 0, OUT: 78, SB: 2520, SP: 6, STACK: [1, 2, 3, 4, 12600, 5, 2520]}
DEBUG:root:{instr: while, value: } {TICK: 452, PC: 3, ADDR: 0, OUT: 78, SB: 2520, SP: 7, STACK: [1, 2, 3, 4, 12600, 5, 2520, 2520]}
DEBUG:root:{instr: tuck, value: } {TICK: 454, PC: 4, ADDR: 0, OUT: 78, SB: 2520, SP: 6, STACK: [1, 2, 3, 4, 12600, 5, 2520]}
DEBUG:root:{instr: mod, value: } {TICK: 462, PC: 5, ADDR: 0, OUT: 78, SB: 2520, SP: 7, STACK: [1, 2, 3, 4, 12600, 2520, 5, 2520]}
DEBUG:root:{instr: repeat, value: } {TICK: 466, PC: 6, ADDR: 0, OUT: 78, SB: 5, SP: 6, STACK: [1, 2, 3, 4, 12600, 2520, 5]}
DEBUG:root:{instr: dup, value: } {TICK: 467, PC: 2, ADDR: 0, OUT: 78, SB: 5, SP: 6, STACK: [1, 2, 3, 4, 12600, 2520, 5]}
DEBUG:root:{instr: while, value: } {TICK: 469, PC: 3, ADDR: 0, OUT: 78, SB: 5, SP: 7, STACK: [1, 2, 3, 4, 12600, 2520, 5, 5]}
DEBUG:root:{instr: tuck, value: } {TICK: 471, PC: 4, ADDR: 0, OUT: 78, SB: 5, SP: 6, STACK: [1, 2, 3, 4, 12600, 2520, 5]}
DEBUG:root:{instr: mod, value: } {TICK: 479, PC: 5, ADDR: 0, OUT: 78, SB: 5, SP: 7, STACK: [1, 2, 3, 4, 12600, 5, 2520, 5]}
DEBUG:root:{instr: repeat, value: } {TICK: 483, PC: 6, ADDR: 0, OUT: 78, SB: 0, SP: 6, STACK: [1, 2, 3, 4, 12600, 5, 0]}
DEBUG:root:{instr: dup, value: } {TICK: 484, PC: 2, ADDR: 0, OUT: 78, SB: 0, SP: 6, STACK: [1, 2, 3, 4, 12600, 5, 0]}
DEBUG:root:{instr: while, value: } {TICK: 486, PC: 3, ADDR: 0, OUT: 78, SB: 0, SP: 7, STACK: [1, 2, 3, 4, 12600, 5, 0, 0]}
DEBUG:root:{instr: drop, value: } {TICK: 489, PC: 7, ADDR: 0, OUT: 78, SB: 0, SP: 6, STACK: [1, 2, 3, 4, 12600, 5, 0]}
DEBUG:root:{instr: ;, value: } {TICK: 490, PC: 8, ADDR: 0, OUT: 78, SB: 0, SP: 5, STACK: [1, 2, 3, 4, 12600, 5]}
DEBUG:root:{instr: /, value: } {TICK: 491, PC: 15, ADDR: 0, OUT: 78, SB: 0, SP: 5, STACK: [1, 2, 3, 4, 12600, 5]}
DEBUG:root:{instr: ;, value: } {TICK: 495, PC: 16, ADDR: 0, OUT: 78, SB: 2520, SP: 4, STACK: [1, 2, 3, 4, 2520]}
DEBUG:root:{instr: word, value: 2} {TICK: 496, PC: 25, ADDR: 0, OUT: 78, SB: 2520, SP: 4, STACK: [1, 2, 3, 4, 2520]}
DEBUG:root:{instr: over, value: } {TICK: 498, PC: 18, ADDR: 0, OUT: 78, SB: 2520, SP: 4, STACK: [1, 2, 3, 4, 2520]}
DEBUG:root:{instr: word, value: 1} {TICK: 500, PC: 19, ADDR: 0, OUT: 78, SB: 4, SP: 5, STACK: [1, 2, 3, 4, 2520, 4]}
DEBUG:root:{instr: =, value: } {TICK: 502, PC: 20, ADDR: 0, OUT: 78, SB: 1, SP: 6, STACK: [1, 2, 3, 4, 2520, 4, 1]}
DEBUG:root:{instr: if, value: } {TICK: 506, PC: 21, ADDR: 0, OUT: 78, SB: 0, SP: 5, STACK: [1, 2, 3, 4, 2520, 0]}
DEBUG:root:{instr: word, value: 1} {TICK: 508, PC: 24, ADDR: 0, OUT: 78, SB: 0, SP: 4, STACK: [1, 2, 3, 4, 2520]}
DEBUG:root:{instr: over, value: } {TICK: 510, PC: 10, ADDR: 0, OUT: 78, SB: 0, SP: 4, STACK: [1, 2, 3, 4, 2520]}
DEBUG:root:{instr: over, value: } {TICK: 512, PC: 11, ADDR: 0, OUT: 78, SB: 4, SP: 5, STACK: [1, 2, 3, 4, 2520, 4]}
DEBUG:root:{instr: *, value: } {TICK: 514, PC: 12, ADDR: 0, OUT: 78, SB: 2520, SP: 6, STACK: [1, 2, 3, 4, 2520, 4, 2520]}
DEBUG:root:{instr: -rot, value: } {TICK: 518, PC: 13, ADDR: 0, OUT: 78, SB: 10080, SP: 5, STACK: [1, 2, 3, 4, 2520, 10080]}
DEBUG:root:{instr: word, value: 0} {TICK: 527, PC: 14, ADDR: 0, OUT: 78, SB: 2520, SP: 5, STACK: [1, 2, 3, 10080, 4, 2520]}
DEBUG:root:{instr: begin, value: } {TICK: 529, PC: 1, ADDR: 0, OUT: 78, SB: 2520, SP: 5, STACK: [1, 2, 3, 10080, 4, 2520]}
DEBUG:root:{instr: dup, value: } {TICK: 531, PC: 2, ADDR: 0, OUT: 78, SB: 2520, SP: 5, STACK: [1, 2, 3, 10080, 4, 2520]}
DEBUG:root:{instr: while, value: } {TICK: 533, PC: 3, ADDR: 0, OUT: 78, SB: 2520, SP: 6, STACK: [1, 2, 3, 10080, 4, 2520, 2520]}
DEBUG:root:{instr: tuck, value: } {TICK: 535, PC: 4, ADDR: 0, OUT: 78, SB: 2520, SP: 5, STACK: [1, 2, 3, 10080, 4, 2520]}
DEBUG:root:{instr: mod, value: } {TICK: 543, PC: 5, ADDR: 0, OUT: 78, SB: 2520, SP: 6, STACK: [1, 2, 3, 10080, 2520, 4, 2520]}
DEBUG:root:{instr: repeat, value: } {TICK: 547, PC: 6, ADDR: 0, OUT: 78, SB: 4, SP: 5, STACK: [1, 2, 3, 10080, 2520, 4]}
DEBUG:root:{instr: dup, value: } {TICK: 548, PC: 2, ADDR: 0, OUT: 78, SB: 4, SP: 5, STACK: [1, 2, 3, 10080, 2520, 4]}
DEBUG:root:{instr: while, value: } {TICK: 550, PC: 3, ADDR: 0, OUT: 78, SB: 4, SP: 6, STACK: [1, 2, 3, 10080, 2520, 4, 4]}
DEBUG:root:{instr: tuck, value: } {TICK: 552, PC: 4, ADDR: 0, OUT: 78, SB: 4, SP: 5, STACK: [1, 2, 3, 10080, 2520, 4]}
DEBUG:root:{instr: mod, value: } {TICK: 560, PC: 5, ADDR: 0, OUT: 78, SB: 4, SP: 6, STACK: [1, 2, 3, 10080, 4, 2520, 4]}
DEBUG:root:{instr: repeat, value: } {TICK: 564, PC: 6, ADDR: 0, OUT: 78, SB: 0, SP: 5, STACK: [1, 2, 3, 10080, 4, 0]}
DEBUG:root:{instr: dup, value: } {TICK: 565, PC: 2, ADDR: 0, OUT: 78, SB: 0, SP: 5, STACK: [1, 2, 3, 10080, 4, 0]}
DEBUG:root:{instr: while, value: } {TICK: 567, PC: 3, ADDR: 0, OUT: 78, SB: 0, SP: 6, STACK: [1, 2, 3, 10080, 4, 0, 0]}
DEBUG:root:{instr: drop, value: } {TICK: 570, PC: 7, ADDR: 0, OUT: 78, SB: 0, SP: 5, STACK: [1, 2, 3, 10080, 4, 0]}
DEBUG:root:{instr: ;, value: } {TICK: 571, PC: 8, ADDR: 0, OUT: 78, SB: 0, SP: 4, STACK: [1, 2, 3, 10080, 4]}
DEBUG:root:{instr: /, value: } {TICK: 572, PC: 15, ADDR: 0, OUT: 78, SB: 0, SP: 4, STACK: [1, 2, 3, 10080, 4]}
DEBUG:root:{instr: ;, value: } {TICK: 576, PC: 16, ADDR: 0, OUT: 78, SB: 2520, SP: 3, STACK: [1, 2, 3, 2520]}
DEBUG:root:{instr: word, value: 2} {TICK: 577, PC: 25, ADDR: 0, OUT: 78, SB: 2520, SP: 3, STACK: [1, 2, 3, 2520]}
DEBUG:root:{instr: over, value: } {TICK: 579, PC: 18, ADDR: 0, OUT: 78, SB: 2520, SP: 3, STACK: [1, 2, 3, 2520]}
DEBUG:root:{instr: word, value: 1} {TICK: 581, PC: 19, ADDR: 0, OUT: 78, SB: 3, SP: 4, STACK: [1, 2, 3, 2520, 3]}
DEBUG:root:{instr: =, value: } {TICK: 583, PC: 20, ADDR: 0, OUT: 78, SB: 1, SP: 5, STACK: [1, 2, 3, 2520, 3, 1]}
DEBUG:root:{instr: if, value: } {TICK: 587, PC: 21, ADDR: 0, OUT: 78, SB: 0, SP: 4, STACK: [1, 2, 3, 2520, 0]}
DEBUG:root:{instr: word, value: 1} {TICK: 589, PC: 24, ADDR: 0, OUT: 78, SB: 0, SP: 3, STACK: [1, 2, 3, 2520]}
DEBUG:root:{instr: over, value: } {TICK: 591, PC: 10, ADDR: 0, OUT: 78, SB: 0, SP: 3, STACK: [1, 2, 3, 2520]}
DEBUG:root:{instr: over, value: } {TICK: 593, PC: 11, ADDR: 0, OUT: 78, SB: 3, SP: 4, STACK: [1, 2, 3, 2520, 3]}
DEBUG:root:{instr: *, value: } {TICK: 595, PC: 12, ADDR: 0, OUT: 78, SB: 2520, SP: 5, STACK: [1, 2, 3, 2520, 3, 2520]}
DEBUG:root:{instr: -rot, value: } {TICK: 599, PC: 13, ADDR: 0, OUT: 78, SB: 7560, SP: 4, STACK: [1, 2, 3, 2520, 7560]}
DEBUG:root:{instr: word, value: 0} {TICK: 608, PC: 14, ADDR: 0, OUT: 78, SB: 2520, SP: 4, STACK: [1, 2, 7560, 3, 2520]}
DEBUG:root:{instr: begin, value: } {TICK: 610, PC: 1, ADDR: 0, OUT: 78, SB: 2520, SP: 4, STACK: [1, 2, 7560, 3, 2520]}
DEBUG:root:{instr: dup, value: } {TICK: 612, PC: 2, ADDR: 0, OUT: 78, SB: 2520, SP: 4, STACK: [1, 2, 7560, 3, 2520]}
DEBUG:root:{instr: while, value: } {TICK: 614, PC: 3, ADDR: 0, OUT: 78, SB: 2520, SP: 5, STACK: [1, 2, 7560, 3, 2520, 2520]}
DEBUG:root:{instr: tuck, value: } {TICK: 616, PC: 4, ADDR: 0, OUT: 78, SB: 2520, SP: 4, STACK: [1, 2, 7560, 3, 2520]}
DEBUG:root:{instr: mod, value: } {TICK: 624, PC: 5, ADDR: 0, OUT: 78, SB: 2520, SP: 5, STACK: [1, 2, 7560, 2520, 3, 2520]}
DEBUG:root:{instr: repeat, value: } {TICK: 628, PC: 6, ADDR: 0, OUT: 78, SB: 3, SP: 4, STACK: [1, 2, 7560, 2520, 3]}
DEBUG:root:{instr: dup, value: } {TICK: 629, PC: 2, ADDR: 0, OUT: 78, SB: 3, SP: 4, STACK: [1, 2, 7560, 2520, 3]}
DEBUG:root:{instr: while, value: } {TICK: 631, PC: 3, ADDR: 0, OUT: 78, SB: 3, SP: 5, STACK: [1, 2, 7560, 2520, 3, 3]}
DEBUG:root:{instr: tuck, value: } {TICK: 633, PC: 4, ADDR: 0, OUT: 78, SB: 3, SP: 4, STACK: [1, 2, 7560, 2520, 3]}
DEBUG:root:{instr: mod, value: } {TICK: 641, PC: 5, ADDR: 0, OUT: 78, SB: 3, SP: 5, STACK: [1, 2, 7560, 3, 2520, 3]}
DEBUG:root:{instr: repeat, value: } {TICK: 645, PC: 6, ADDR: 0, OUT: 78, SB: 0, SP: 4, STACK: [1, 2, 7560, 3, 0]}
DEBUG:root:{instr: dup, value: } {TICK: 646, PC: 2, ADDR: 0, OUT: 78, SB: 0, SP: 4, STACK: [1, 2, 7560, 3, 0]}
DEBUG:root:{instr: while, value: } {TICK: 648, PC: 3, ADDR: 0, OUT: 78, SB: 0, SP: 5, STACK: [1, 2, 7560, 3, 0, 0]}
DEBUG:root:{instr: drop, value: } {TICK: 651, PC: 7, ADDR: 0, OUT: 78, SB: 0, SP: 4, STACK: [1, 2, 7560, 3, 0]}
DEBUG:root:{instr: ;, value: } {TICK: 652, PC: 8, ADDR: 0, OUT: 78, SB: 0, SP: 3, STACK: [1, 2, 7560, 3]}
DEBUG:root:{instr: /, value: } {TICK: 653, PC: 15, ADDR: 0, OUT: 78, SB: 0, SP: 3, STACK: [1, 2, 7560, 3]}
DEBUG:root:{instr: ;, value: } {TICK: 657, PC: 16, ADDR: 0, OUT: 78, SB: 2520, SP: 2, STACK: [1, 2, 2520]}
DEBUG:root:{instr: word, value: 2} {TICK: 658, PC: 25, ADDR: 0, OUT: 78, SB: 2520, SP: 2, STACK: [1, 2, 2520]}
DEBUG:root:{instr: over, value: } {TICK: 660, PC: 18, ADDR: 0, OUT: 78, SB: 2520, SP: 2, STACK: [1, 2, 2520]}
DEBUG:root:{instr: word, value: 1} {TICK: 662, PC: 19, ADDR: 0, OUT: 78, SB: 2, SP: 3, STACK: [1, 2, 2520, 2]}
DEBUG:root:{instr: =, value: } {TICK: 664, PC: 20, ADDR: 0, OUT: 78, SB: 1, SP: 4, STACK: [1, 2, 2520, 2, 1]}
DEBUG:root:{instr: if, value: } {TICK: 668, PC: 21, ADDR: 0, OUT: 78, SB: 0, SP: 3, STACK: [1, 2, 2520, 0]}
DEBUG:root:{instr: word, value: 1} {TICK: 670, PC: 24, ADDR: 0, OUT: 78, SB: 0, SP: 2, STACK: [1, 2, 2520]}
DEBUG:root:{instr: over, value: } {TICK: 672, PC: 10, ADDR: 0, OUT: 78, SB: 0, SP: 2, STACK: [1, 2, 2520]}
DEBUG:root:{instr: over, value: } {TICK: 674, PC: 11, ADDR: 0, OUT: 78, SB: 2, SP: 3, STACK: [1, 2, 2520, 2]}
DEBUG:root:{instr: *, value: } {TICK: 676, PC: 12, ADDR: 0, OUT: 78, SB: 2520, SP: 4, STACK: [1, 2, 2520, 2, 2520]}
DEBUG:root:{instr: -rot, value: } {TICK: 680, PC: 13, ADDR: 0, OUT: 78, SB: 5040, SP: 3, STACK: [1, 2, 2520, 5040]}
DEBUG:root:{instr: word, value: 0} {TICK: 689, PC: 14, ADDR: 0, OUT: 78, SB: 2520, SP: 3, STACK: [1, 5040, 2, 2520]}
DEBUG:root:{instr: begin, value: } {TICK: 691, PC: 1, ADDR: 0, OUT: 78, SB: 2520, SP: 3, STACK: [1, 5040, 2, 2520]}
DEBUG:root:{instr: dup, value: } {TICK: 693, PC: 2, ADDR: 0, OUT: 78, SB: 2520, SP: 3, STACK: [1, 5040, 2, 2520]}
DEBUG:root:{instr: while, value: } {TICK: 695, PC: 3, ADDR: 0, OUT: 78, SB: 2520, SP: 4, STACK: [1, 5040, 2, 2520, 2520]}
DEBUG:root:{instr: tuck, value: } {TICK: 697, PC: 4, ADDR: 0, OUT: 78, SB: 2520, SP: 3, STACK: [1, 5040, 2, 2520]}
DEBUG:root:{instr: mod, value: } {TICK: 705, PC: 5, ADDR: 0, OUT: 78, SB: 2520, SP: 4, STACK: [1, 5040, 2520, 2, 2520]}
DEBUG:root:{instr: repeat, value: } {TICK: 709, PC: 6, ADDR: 0, OUT: 78, SB: 2, SP: 3, STACK: [1, 5040, 2520, 2]}
DEBUG:root:{instr: dup, value: } {TICK: 710, PC: 2, ADDR: 0, OUT: 78, SB: 2, SP: 3, STACK: [1, 5040, 2520, 2]}
DEBUG:root:{instr: while, value: } {TICK: 712, PC: 3, ADDR: 0, OUT: 78, SB: 2, SP: 4, STACK: [1, 5040, 2520, 2, 2]}
DEBUG:root:{instr: tuck, value: } {TICK: 714, PC: 4, ADDR: 0, OUT: 78, SB: 2, SP: 3, STACK: [1, 5040, 2520, 2]}
DEBUG:root:{instr: mod, value: } {TICK: 722, PC: 5, ADDR: 0, OUT: 78, SB: 2, SP: 4, STACK: [1, 5040, 2, 2520, 2]}
DEBUG:root:{instr: repeat, value: } {TICK: 726, PC: 6, ADDR: 0, OUT: 78, SB: 0, SP: 3, STACK: [1, 5040, 2, 0]}
DEBUG:root:{instr: dup, value: } {TICK: 727, PC: 2, ADDR: 0, OUT: 78, SB: 0, SP: 3, STACK: [1, 5040, 2, 0]}
DEBUG:root:{instr: while, value: } {TICK: 729, PC: 3, ADDR: 0, OUT: 78, SB: 0, SP: 4, STACK: [1, 5040, 2, 0, 0]}
DEBUG:root:{instr: drop, value: } {TICK: 732, PC: 7, ADDR: 0, OUT: 78, SB: 0, SP: 3, STACK: [1, 5040, 2, 0]}
DEBUG:root:{instr: ;, value: } {TICK: 733, PC: 8, ADDR: 0, OUT: 78, SB: 0, SP: 2, STACK: [1, 5040, 2]}
DEBUG:root:{instr: /, value: } {TICK: 734, PC: 15, ADDR: 0, OUT: 78, SB: 0, SP: 2, STACK: [1, 5040, 2]}
DEBUG:root:{instr: ;, value: } {TICK: 738, PC: 16, ADDR: 0, OUT: 78, SB: 2520, SP: 1, STACK: [1, 2520]}
DEBUG:root:{instr: word, value: 2} {TICK: 739, PC: 25, ADDR: 0, OUT: 78, SB: 2520, SP: 1, STACK: [1, 2520]}
DEBUG:root:{instr: over, value: } {TICK: 741, PC: 18, ADDR: 0, OUT: 78, SB: 2520, SP: 1, STACK: [1, 2520]}
DEBUG:root:{instr: word, value: 1} {TICK: 743, PC: 19, ADDR: 0, OUT: 78, SB: 1, SP: 2, STACK: [1, 2520, 1]}
DEBUG:root:{instr: =, value: } {TICK: 745, PC: 20, ADDR: 0, OUT: 78, SB: 1, SP: 3, STACK: [1, 2520, 1, 1]}
DEBUG:root:{instr: if, value: } {TICK: 749, PC: 21, ADDR: 0, OUT: 78, SB: 1, SP: 2, STACK: [1, 2520, 1]}
DEBUG:root:{instr: *, value: } {TICK: 751, PC: 22, ADDR: 0, OUT: 78, SB: 1, SP: 1, STACK: [1, 2520]}
DEBUG:root:{instr: ;, value: } {TICK: 755, PC: 23, ADDR: 0, OUT: 78, SB: 2520, SP: 0, STACK: [2520]}
DEBUG:root:{instr: ;, value: } {TICK: 756, PC: 27, ADDR: 0, OUT: 78, SB: 2520, SP: 0, STACK: [2520]}
DEBUG:root:{instr: ;, value: } {TICK: 757, PC: 26, ADDR: 0, OUT: 78, SB: 2520, SP: 0, STACK: [2520]}
DEBUG:root:{instr: ;, value: } {TICK: 758, PC: 27, ADDR: 0, OUT: 78, SB: 2520, SP: 0, STACK: [2520]}
DEBUG:root:{instr: ;, value: } {TICK: 759, PC: 26, ADDR: 0, OUT: 78, SB: 2520, SP: 0, STACK: [2520]}
DEBUG:root:{instr: ;, value: } {TICK: 760, PC: 27, ADDR: 0, OUT: 78, SB: 2520, SP: 0, STACK: [2520]}
DEBUG:root:{instr: ;, value: } {TICK: 761, PC: 26, ADDR: 0, OUT: 78, SB: 2520, SP: 0, STACK: [2520]}
DEBUG:root:{instr: ;, value: } {TICK: 762, PC: 27, ADDR: 0, OUT: 78, SB: 2520, SP: 0, STACK: [2520]}
DEBUG:root:{instr: ;, value: } {TICK: 763, PC: 26, ADDR: 0, OUT: 78, SB: 2520, SP: 0, STACK: [2520]}
DEBUG:root:{instr: ;, value: } {TICK: 764, PC: 27, ADDR: 0, OUT: 78, SB: 2520, SP: 0, STACK: [2520]}
DEBUG:root:{instr: ;, value: } {TICK: 765, PC: 26, ADDR: 0, OUT: 78, SB: 2520, SP: 0, STACK: [2520]}
DEBUG:root:{instr: ;, value: } {TICK: 766, PC: 27, ADDR: 0, OUT: 78, SB: 2520, SP: 0, STACK: [2520]}
DEBUG:root:{instr: ;, value: } {TICK: 767, PC: 26, ADDR: 0, OUT: 78, SB: 2520, SP: 0, STACK: [2520]}
DEBUG:root:{instr: ;, value: } {TICK: 768, PC: 27, ADDR: 0, OUT: 78, SB: 2520, SP: 0, STACK: [2520]}
DEBUG:root:{instr: ;, value: } {TICK: 769, PC: 26, ADDR: 0, OUT: 78, SB: 2520, SP: 0, STACK: [2520]}
DEBUG:root:{instr: ;, value: } {TICK: 770, PC: 27, ADDR: 0, OUT: 78, SB: 2520, SP: 0, STACK: [2520]}
DEBUG:root:{instr: ;, value: } {TICK: 771, PC: 26, ADDR: 0, OUT: 78, SB: 2520, SP: 0, STACK: [2520]}
DEBUG:root:{instr: ;, value: } {TICK: 772, PC: 27, ADDR: 0, OUT: 78, SB: 2520, SP: 0, STACK: [2520]}
DEBUG:root:{instr: ., value: } {TICK: 773, PC: 39, ADDR: 0, OUT: 78, SB: 2520, SP: 0, STACK: [2520]}
INFO:root:output_buffer: '[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]'
instr_counter:  281 ticks: 773
```

| алг.  | LoC | code байт | code инстр. | такт. | вариант |
|-------|-----|-----------|-------------|-------|---------|
| hello | 25  | -         | 25          | 36    | `forth | stack | harv | hw | instr | struct | trap | mem | prob5`     |
| cat   | 22  | -         | 20          | 3195  | `forth | stack | harv | hw | instr | struct | trap | mem | prob5`     |
| prob5 | 43  | -         | 40          | 773   | `forth | stack | harv | hw | instr | struct | trap | mem | prob5`     |


