"""
ISA
"""

import json
from collections import namedtuple


keywords = [">", "<", ">=", "<=", "=", "!=", "+", "-", "*", "/", "mod", "begin", "while", "repeat",
            "dup", "over", "drop", "tuck", "rot", "-rot", ".", ";", "CR", "KEY"]

stack_mut_operations = [">", "<", ">=", "<=", "=", "!=", "+", "-", "*", "/", "mod",
                        "while", "dup", "over", "drop", "tuck", "rot", "-rot", "word",
                        "CR", "KEY"]


class Term(namedtuple('Term', 'line pos symbol')):
    """Описание выражения из исходного текста программы."""
    # сделано через класс, чтобы был docstring


def write_code(filename, code):
    """Записать машинный код в файл."""
    with open(filename, "w", encoding="utf-8") as file:
        file.write(json.dumps(code, indent=4))


def read_code(filename):
    """Прочесть машинный код из файла."""
    with open(filename, encoding="utf-8") as file:
        code = json.loads(file.read())

    for instr in code:
        if 'term' in instr:
            instr['term'] = Term(
                instr['term'][0], instr['term'][1], instr['term'][2])

    return code
