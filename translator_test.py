"""
Unit-тесты для транслятора
"""

import unittest
import translator
import isa


class Test(unittest.TestCase):
    """
    Unit-тесты для транслятора
    """

    def simple_test(self, input_file, output, correct):
        translator.main([input_file, output])

        result_code = isa.read_code(output)
        correct_code = isa.read_code(correct)

        self.assertTrue(result_code, correct_code)

    def test_recursive(self):
        self.simple_test("tests/test_recursive", "tests/test_recursive.f",
                         "tests/test_recursive_correct.f")

    def test_prob5(self):
        self.simple_test("tests/prob5_program", "tests/prob5.f", "tests/prob5_correct.f")

    def test_hello(self):
        self.simple_test("tests/hello_program", "tests/hello.f", "tests/hello.f")


if __name__ == '__main__':
    unittest.main()
