"""
Unit-тесты для процессора
"""
import unittest
import machine
import translator


def start_machine(input_file, output):
    input_tokens = "tests/input_file"
    translator.main([input_file, output])
    return machine.main([output, input_tokens])


class Test(unittest.TestCase):
    """
    Unit-тесты для процессора
    """

    def test_simple(self):
        stack = start_machine("tests/simple_test_program", "tests/simple_test.f")
        self.assertTrue(stack[0], 70)

    def test_not_so_simple(self):
        stack = start_machine("tests/not_so_simple_program", "tests/not_so_simple.f")
        self.assertTrue(stack[0], 1400)

    def test_dup(self):
        stack = start_machine("tests/dup", "tests/dup.f")
        self.assertTrue(stack[0], 1)
        self.assertTrue(stack[1], 2)
        self.assertTrue(stack[2], 3)
        self.assertTrue(stack[3], 3)

    def test_if(self):
        stack = start_machine("tests/if_program", "tests/if.f")
        # print(f"stack = {stack}")
        self.assertTrue(stack[0], 5)

    def test_while(self):
        stack = start_machine("tests/while_program", "tests/while.f")
        self.assertTrue(stack, stack)

    def test_prob5(self):
        stack = start_machine("tests/prob5_program", "tests/prob5.f")
        # print(f"stack = {stack}")
        self.assertTrue(stack[0], 2520)

    def test_out(self):
        stack = start_machine("tests/out_program", "tests/out.f")
        self.assertTrue(stack, stack)

    def test_cat(self):
        stack = start_machine("tests/cat_program", "tests/cat.f")
        self.assertTrue(stack, stack)

    def test_hello(self):
        stack = start_machine("tests/hello_program", "tests/hello.f")
        self.assertTrue(stack, stack)


if __name__ == '__main__':
    unittest.main()
