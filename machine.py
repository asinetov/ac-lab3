#!/usr/bin/python3
# pylint: disable=missing-function-docstring  # чтобы не быть Капитаном Очевидностью
# pylint: disable=invalid-name                # сохраним традиционные наименования сигналов
# pylint: disable=consider-using-f-string     # избыточный синтаксис

"""
Модель процессора, позволяющая выполнить странслированные программы на языке Forth.
"""
import logging
import sys
from isa import read_code


class DataPath:
    """
    модель работы DataPath
    """

    def __init__(self, data_memory_size: int, stack_size: int, input_buffer: list):

        assert data_memory_size > 256, "Data_memory size should be non-zero"
        assert stack_size > 0, "stack size should be non-zero"

        # 0...127 -- memory-mapped input
        # 128..255 -- memory-mapped output
        self.data_memory_size = data_memory_size
        self.data_memory = [0] * data_memory_size
        self.data_address = 0

        self.input_pointer = 0
        self.output_pointer = 0

        self.stack_buffer = 0
        self.stack_pointer = -1
        self.stack = [0] * stack_size

        self.back_stack = [0] * 1024
        self.back_stack_pointer = -1

        self.interrupts = [0] * 128
        self.interrupt_pointer = 0
        self.len_input = len(input_buffer) // 2
        # self.ready = 0
        # self.io = 0

        for i in range(0, min(128, len(input_buffer))):
            if i % 2 == 0:
                self.interrupts[i // 2] = int(input_buffer[i])
            else:
                self.data_memory[i // 2] = ord(input_buffer[i])

        print(self.interrupts)

    def latch_data_addr(self, addr: int):
        self.data_address = addr

    def latch_increment_data_addr(self):
        self.data_address += 1 % self.data_memory_size

    def latch_stack_buffer(self, value: int):
        self.stack_buffer = value

    def latch_increase_stack_pointer(self, value: int):
        self.stack_pointer += value

    def latch_increase_back_stack_pointer(self, value: int):
        self.back_stack_pointer += value

    def read_top(self, offset: int) -> int:
        """
        Прочитать значение с вершины стека в стек-буффер
        """
        assert offset >= 0
        return self.stack[self.stack_pointer - offset]

    def read_back_top(self, offset: int) -> int:
        """
        Прочитать значение с вершины стека контекста
        """
        assert offset >= 0
        return self.back_stack[self.back_stack_pointer - offset]

    def write_top(self, value: int, offset: int):
        """
        Записать значение из стек-буффера на вершину стека
        """
        assert offset >= 0
        self.stack[self.stack_pointer - offset] = value

    def push(self):
        self.latch_increase_stack_pointer(1)
        self.stack[self.stack_pointer] = self.stack_buffer

    def pop(self) -> int:
        assert self.stack_pointer >= 0
        value = self.stack[self.stack_pointer]
        self.latch_increase_stack_pointer(-1)
        return value

    def save_context(self, program_counter: int):
        self.latch_increase_back_stack_pointer(1)
        self.back_stack[self.back_stack_pointer] = program_counter

    def restore_context(self) -> int:
        self.latch_increase_back_stack_pointer(-1)
        return self.back_stack[self.back_stack_pointer + 1]

    def output(self, offset: int):
        symbol = self.read_top(offset)
        self.data_memory[128 + self.output_pointer] = symbol
        smb = chr(symbol)
        print(f"output: {smb}")
        self.output_pointer += 1 % 128

    def write(self, offset: int):

        if 128 <= self.data_address < 256:
            self.output(offset)
        elif 0 <= self.data_address < 128:
            raise Exception
        else:
            self.data_memory[self.data_address] = self.read_top(offset)

    def input(self) -> int:
        self.input_pointer += 1 % 128
        return self.data_memory[self.input_pointer - 1]

    def read(self):

        if 0 <= self.data_address < 128:
            self.latch_stack_buffer(self.input())
            self.push()
        elif 128 <= self.data_address < 256:
            raise Exception
        else:
            self.latch_stack_buffer(self.data_memory[self.data_address])
            self.push()

    def zero(self) -> bool:
        return self.stack[self.stack_pointer] == 0


class ControlUnit:
    """
    модель работы ControlUnit
    """

    def __init__(self, program: list, data_path: DataPath):
        self.program = program
        self.program_counter = 0
        self.data_path = data_path
        self._tick = 0
        self.in_interrupt = 0
        self.user_functions = {}

    def tick(self):
        """Счётчик тактов процессора. Вызывается при переходе на следующий такт."""
        self._tick += 1

    def current_tick(self):
        return self._tick

    def latch_program_counter(self, jmp: bool, jmp_addr: int):
        if not jmp:
            self.program_counter += 1
        else:
            self.program_counter = jmp_addr

    def interrupt(self):
        """
        Обработчик прерываний
        """
        if 0 not in self.user_functions:
            return

        if self.data_path.interrupt_pointer > self.data_path.len_input:
            raise StopIteration

        if self.in_interrupt == 0 \
                and self._tick >= self.data_path.interrupts[self.data_path.interrupt_pointer]:
            self.in_interrupt = 1
            self.data_path.interrupt_pointer += 1
            # call read
            self.data_path.save_context(self.program_counter)
            self.latch_program_counter(True, self.user_functions[0])

    def swap(self):
        """
        dry-функция, которая меняет аргументы на стеке местами
        """
        value_1 = self.data_path.pop()
        value_2 = self.data_path.pop()

        self.data_path.latch_stack_buffer(value_1)
        self.data_path.push()

        self.data_path.latch_stack_buffer(value_2)
        self.data_path.push()

    def push3(self, value_1: int, value_2: int, value_3: int):
        """
        dry-функция, которая кладет на стек 3 аргумента
        """
        self.data_path.latch_stack_buffer(value_1)
        self.data_path.push()

        self.data_path.latch_stack_buffer(value_2)
        self.data_path.push()

        self.data_path.latch_stack_buffer(value_3)
        self.data_path.push()

    def decode_and_execute_instruction(self):

        instr = self.program[self.program_counter]
        self.latch_program_counter(False, 0)
        self.execute(instr)

        self.tick()
        self.interrupt()

    def execute(self, instr):

        opcode = instr["opcode"]

        if opcode == ".":
            raise StopIteration()

        if opcode == "dup":
            self.data_path.latch_stack_buffer(self.data_path.read_top(0))
            self.data_path.push()

        if opcode == "over":
            self.data_path.latch_stack_buffer(self.data_path.read_top(1))
            self.data_path.push()

        if opcode == "drop":
            self.data_path.pop()

        if opcode in ("+", "-", "*", "/", "mod", ">", "<", "=", "!="):
            value_1 = self.data_path.pop()
            value_2 = self.data_path.pop()
            if opcode == "+":
                self.data_path.latch_stack_buffer(value_1 + value_2)
            elif opcode == "*":
                self.data_path.latch_stack_buffer(value_1 * value_2)
            elif opcode == "-":
                self.data_path.latch_stack_buffer(value_2 - value_1)
            elif opcode == "/":
                self.data_path.latch_stack_buffer(value_2 // value_1)
            elif opcode == "mod":
                self.data_path.latch_stack_buffer(value_2 % value_1)
            elif opcode == ">":
                self.data_path.latch_stack_buffer(int(value_2 > value_1))
            elif opcode == "<":
                self.data_path.latch_stack_buffer(int(value_2 < value_1))
            elif opcode == ">=":
                self.data_path.latch_stack_buffer(int(value_2 >= value_1))
            elif opcode == "<=":
                self.data_path.latch_stack_buffer(int(value_2 <= value_1))
            elif opcode == "=":
                self.data_path.latch_stack_buffer(int(value_2 == value_1))
            elif opcode == "!=":
                self.data_path.latch_stack_buffer(int(value_2 != value_1))
            self.data_path.push()

        if opcode in ('swap', 'tuck'):

            self.swap()

            if opcode == "tuck":
                self.data_path.latch_stack_buffer(self.data_path.read_top(1))
                self.data_path.push()

        if opcode in ('rot', '-rot'):

            value_1 = self.data_path.pop()
            value_2 = self.data_path.pop()
            value_3 = self.data_path.pop()

            if opcode == "rot":
                self.push3(value_2, value_1, value_3)
            else:
                self.push3(value_1, value_3, value_2)

        if opcode == "function":
            # address of first instruction inside the function
            self.user_functions[int(instr['name'])] = self.program_counter
            self.latch_program_counter(True, self.program_counter + int(instr['body-size']))

        if opcode == "word" and instr["type"] == "int":
            value = int(instr["value"])
            self.data_path.latch_stack_buffer(value)
            self.data_path.push()

        if opcode == "word" and instr["type"] == "quoted":
            value = ord(str(instr["value"]))
            self.data_path.latch_stack_buffer(value)
            self.data_path.push()

        # call function
        if opcode == "word" and instr["type"] == "func":
            self.data_path.save_context(self.program_counter)
            if instr["value"] not in self.user_functions:
                raise Exception
            self.latch_program_counter(True, self.user_functions[instr["value"]])

        if opcode == "if":
            self.data_path.save_context(
                self.program_counter + int(instr['true-size']) + int(instr['false-size']))
            condition = self.data_path.zero()
            self.data_path.pop()
            if condition:  # false
                self.latch_program_counter(True, self.program_counter + int(instr['true-size']))

        # while
        if opcode == "begin":
            self.data_path.save_context(
                self.program_counter + int(instr['begin-size']) + int(instr['while-size']))
            self.data_path.save_context(self.program_counter)

        if opcode == "while":
            condition = self.data_path.zero()
            self.data_path.pop()
            if condition:  # false
                self.data_path.restore_context()
                self.latch_program_counter(True, self.data_path.restore_context())

        if opcode == "repeat":
            self.latch_program_counter(True, self.data_path.read_back_top(0))

        if opcode == ";":
            self.in_interrupt = 0
            self.latch_program_counter(True, self.data_path.restore_context())

        if opcode == "CR":
            self.data_path.latch_data_addr(128)
            self.data_path.write(0)
            self.data_path.pop()

        if opcode == "KEY":
            self.data_path.latch_data_addr(0)
            self.data_path.read()

    def __repr__(self):
        repr_stack = []
        if self.data_path.stack_pointer >= 0:
            begin = max(0, self.data_path.stack_pointer - 10)
            repr_stack = self.data_path.stack[begin:self.data_path.stack_pointer + 1]

        state = "{{INT {}, TICK: {}, PC: {}, ADDR: {}, OUT: {}, SB: {}, SP: {}, STACK: {}}}".format(
            self.in_interrupt,
            self._tick,
            self.program_counter,
            self.data_path.data_address,
            self.data_path.data_memory[self.data_path.data_address],
            self.data_path.stack_buffer,
            self.data_path.stack_pointer,
            repr_stack,
        )

        instr = self.program[self.program_counter]
        opcode = instr["opcode"]
        value = instr.get("value", "")
        action = "{{instr: {}, value: {}}}".format(opcode, value)
        return "{} {}".format(action, state)


def simulation(code, input_tokens, data_memory_size, stack_size):
    """
    Запуск симуляции процессора.
    """
    data_path = DataPath(data_memory_size, stack_size, input_tokens)
    control_unit = ControlUnit(code, data_path)
    instr_counter = 0

    logging.debug('%s', control_unit)
    try:
        while True:
            control_unit.decode_and_execute_instruction()
            instr_counter += 1
            logging.debug('%s', control_unit)
    except EOFError:
        logging.warning('Input buffer is empty!')
    except StopIteration:
        pass

    logging.info('output_buffer: %s', repr(''.join(str(data_path.data_memory[128:256]))))
    return ''.join(str(data_path.data_memory[128:256])), \
           instr_counter, control_unit.current_tick(), \
           control_unit.data_path.stack


def main(args):
    logging.getLogger().setLevel(logging.DEBUG)

    assert len(args) == 2, "Wrong arguments: machine.py <code_file> <input_file>"
    code_file, input_file = args

    code = read_code(code_file)
    print(code)

    input_token = []
    if input_file != '':
        with open(input_file, encoding="utf-8") as file:
            input_text = file.read()
            input_token = input_text.split()

    print(input_token)

    output, instr_counter, ticks, stack = simulation(code,
                                                     input_tokens=input_token,
                                                     data_memory_size=1024,
                                                     stack_size=64)

    print(''.join(output))
    print("instr_counter: ", instr_counter, "ticks:", ticks)
    return stack


if __name__ == '__main__':
    main(sys.argv[1:])
