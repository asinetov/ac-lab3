"""
Модуль трансляции
"""

import sys
from isa import write_code, Term, keywords


class ListStream:
    """
    Делает из набора термов поток для удобной работы с вводом
    """

    def __init__(self, lst: list):
        self.__lst__ = lst
        self.__idx__ = 0

    def next(self):
        self.__idx__ += 1

    def get_next(self):
        return self.__lst__[self.__idx__ + 1]

    def get(self):
        return self.__lst__[self.__idx__]

    def is_end(self):
        return self.__idx__ >= len(self.__lst__)


FUNCTION_ID = 1
NAME_ID_MAP = {}


def read_operation(terms: ListStream, this_list: list) -> dict:
    term = terms.get().symbol
    this_obj = {}

    if term == ":":
        this_obj['opcode'] = "function"
        terms.next()
        global FUNCTION_ID
        if terms.get().symbol != "read":
            this_obj['name'] = FUNCTION_ID
        else:
            this_obj['name'] = 0
        NAME_ID_MAP[terms.get().symbol] = FUNCTION_ID
        FUNCTION_ID += 1

    elif term == "IF":
        this_obj['opcode'] = "if"

    elif term == "begin":
        this_obj['opcode'] = "begin"

    elif term in keywords:
        this_obj['opcode'] = term

    else:
        this_obj['opcode'] = "word"
        if term.isdigit():
            this_obj['type'] = "int"
            this_obj['value'] = term
        elif term[0] == "\"" and term[len(term) - 1] == "\"":
            this_op = {'opcode': terms.get_next().symbol}
            i = 0
            while True:
                this_obj['type'] = "quoted"
                this_obj['value'] = term[i + 1]
                if term[i + 2] == "\"":
                    break
                this_list.append(this_obj)
                if this_op['opcode'] == "CR":
                    this_list.append(this_op)
                i += 1
                this_obj = {'opcode': "word"}

        else:
            if term not in NAME_ID_MAP:
                raise Exception
            this_obj['type'] = "func"
            this_obj['value'] = NAME_ID_MAP[term]

    return this_obj


def fill(terms: ListStream, this_obj: list, name: str, this_len: int, new_len: int):
    this_obj = read_word(terms, this_obj)
    this_obj[this_len - 1][name] = len(this_obj) - new_len


def read_word(terms: ListStream, this_obj: list) -> list:
    """
    Рекурсивная функция обработки программы.
    Перебираем термы. Если встречаем опеределение функции, цикл или условие,
    запускаемся рекурсивно
    """
    while not terms.is_end():

        if terms.get().symbol in (";", "else", "endif", "while", "repeat"):
            if terms.get().symbol == "repeat":
                this_obj.append({'opcode': 'repeat'})
            elif terms.get().symbol == "while":
                this_obj.append({'opcode': 'while'})
            else:
                this_obj.append({'opcode': ';'})
            terms.next()
            return this_obj

        operation = read_operation(terms, this_obj)
        this_obj.append(operation)
        terms.next()

        this_len = len(this_obj)
        if operation['opcode'] == 'function':
            fill(terms, this_obj, 'body-size', this_len, this_len)
        elif operation['opcode'] == 'if':
            fill(terms, this_obj, 'true-size', this_len, this_len)
            new_len = len(this_obj)
            fill(terms, this_obj, 'false-size', this_len, new_len)
        elif operation['opcode'] == 'begin':
            fill(terms, this_obj, 'begin-size', this_len, this_len)
            new_len = len(this_obj)
            fill(terms, this_obj, 'while-size', this_len, new_len)

    return this_obj


def translate(text: str) -> list:
    """
    Транслируем текст в последовательность значимых термов.
    """

    terms = []
    for line_num, line in enumerate(text.split('\n'), 1):
        comment = False
        for pos, word in enumerate(line.split(), 1):
            if word == '\\':
                comment = True
            if not (word[0] == '<' and len(word) > 1) and not comment:
                terms.append(Term(line_num, pos, word))

    stack = []

    assert terms[len(terms) - 1].symbol == ".", "End of program"

    for term in terms:
        if term.symbol in (":", "if", "begin"):
            stack.append(term.symbol)
        elif term.symbol == ";":
            if stack[len(stack) - 1] == ":" or stack[len(stack) - 1] == "elif":
                stack.pop()
            elif stack[len(stack) - 1] == "if":
                stack[len(stack) - 1] = "elif"
            else:
                assert False, "unbalanced functions"
        elif term.symbol == "while":
            assert stack[len(stack) - 1] == "begin", "unbalanced while"
            stack[len(stack) - 1] = "while"
        elif term.symbol == "repeat":
            assert stack[len(stack) - 1] == "while", "unbalanced while"
            stack.pop()

    # Транслируем термы в машинный код.
    code = read_word(ListStream(terms), [])

    global FUNCTION_ID
    global NAME_ID_MAP
    FUNCTION_ID = 1
    NAME_ID_MAP = {}

    return code


def main(args):
    assert len(args) == 2, \
        "Wrong arguments: translator.py <input_file> <target_file>"
    source, target = args

    with open(source, "rt", encoding="utf-8") as read_file:
        source = read_file.read()

    code = translate(source)

    print("source LoC:", len(source.split()), "code instr:", len(code))
    write_code(target, code)


if __name__ == '__main__':
    main(sys.argv[1:])
